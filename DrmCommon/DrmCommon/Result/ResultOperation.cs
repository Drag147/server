﻿using System.Collections.Generic;

namespace DrmCommon.Result
{
    public enum Operation
    {
        None,


        BuyContent,
        DeleteContent,

        GetContentsForUser,
        GetAllContents,
        GetOpenContent,

        ConvertContentOnServer,
        UploadContent,

        AddNewSessionKey,
        GenerateNewContent
    }

    public class ResultOperation
    {
        public Operation operation;
        public bool success { get; set; }
        public List<string> ErrorMessages { get; set; }
        public List<string> InfoMessages { get; set; }

        public ResultOperation()
        {
            this.operation = Operation.None;
            this.success = false;
            this.ErrorMessages = new List<string>();
            this.InfoMessages = new List<string>();
        }

        public void AddNewErrorMessage(string ErrorMessage)
        {
            this.ErrorMessages.Add(ErrorMessage);
        }

        public void ClearErrorMessages()
        {
            this.ErrorMessages.Clear();
        }

        public void AddNewInfoMessage(string InfoMessage)
        {
            this.InfoMessages.Add(InfoMessage);
        }

        public void ClearInfoMessages()
        {
            this.InfoMessages.Clear();
        }
    }
}
