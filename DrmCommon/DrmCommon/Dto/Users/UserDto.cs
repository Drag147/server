﻿namespace DrmCommon.Dto.Users
{
    public class UserDto : ObjectDto
    {
        public string login { get; set; }
        public string passwordHash { get; set; }

        public string name { get; set; }
        public string surname { get; set; }
        public string patronymic { get; set; }
        public string mail { get; set; }

        public UserDto()
        {

        }

        public UserDto(string login, string passwordHash)
        {
            this.login = login;
            this.passwordHash = passwordHash;
        }
    }
}
