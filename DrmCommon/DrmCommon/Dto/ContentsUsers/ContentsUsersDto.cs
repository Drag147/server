﻿namespace DrmCommon.Dto.ContentsUsers
{
    public class ContentsUsersDto : ObjectDto
    {
        public int idContentsUsers { get; set; }
        public int idUser { get; set; }
        public int idContent { get; set; }

        public ContentsUsersDto()
        {

        }
    }
}
