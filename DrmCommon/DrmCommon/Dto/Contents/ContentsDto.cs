﻿namespace DrmCommon.Dto.Contents
{
    public class ContentsDto : ObjectDto
    {
        public int id { get; set; }
        public string NameContent { get; set; }
        public byte[] ContentData { get; set; }
        public uint sizeContent { get; set; }

        public ContentsDto()
        {

        }
    }
}
