﻿using DrmCommon.Helper;

namespace DrmCommon.Dto.SessionKey
{
    public class SessionKeyGenerateDto : ObjectDto
    {
        public int IDKey { get; set; }
        public BigInteger N { get; set; }
        public BigInteger G { get; set; }
        public BigInteger X { get; set; }
        public BigInteger Y { get; set; }

        public SessionKeyGenerateDto()
        {

        }
    }
}
