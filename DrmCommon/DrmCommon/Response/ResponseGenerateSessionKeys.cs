﻿using DrmCommon.Dto.SessionKey;

namespace DrmCommon.Response
{
    public class ResponseGenerateSessionKeys : Response
    {
        public SessionKeyGenerateDto sessionKeyGenerateDto { get; set; }

        public ResponseGenerateSessionKeys()
        {

        }
    }
}
