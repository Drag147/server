﻿using DrmCommon.Result;

namespace DrmCommon.Response
{
    public enum TypeResponse
    {
        None,

        GenerateSessionKeyStep1, //Первый шаг генерации Сеансовых ключей
        GenerateSessionKeyStep2, //Второй шаг генерации Сеансовых ключей
        RequestAuthorization, //Отправка данных для авторизации
        ResultAuthorization, //Результат авторизации

        Registration,
        CheckAccess,
        ChangePassword,

        BuyContent,
        DeleteContent,
        GetListContentsAll,
        GetListUserContents,
        GetOpenContent
    }

    //С сервера
    public class Response
    {
        public TypeResponse typeResponse { get; set; }
        public ResultOperation resultOperation { get; set; }
        public bool NeedUpdateKey { get; set; }

        public Response()
        {
            typeResponse = TypeResponse.None;
            NeedUpdateKey = false;
        }
    }
}
