﻿using DrmCommon.Dto.Contents;

namespace DrmCommon.Response
{
    public class ResponseGetOpenContent : Response
    {
        public string encriptedKeyForContent { get; set; }
        public ContentsDto content { get; set; }
        public int sendedSizeContent { get; set; }

        public ResponseGetOpenContent()
        {
            encriptedKeyForContent = null;
        }
    }
}
