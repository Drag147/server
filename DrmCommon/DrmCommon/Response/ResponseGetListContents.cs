﻿using DrmCommon.Dto.Contents;
using System.Collections.Generic;

namespace DrmCommon.Response
{
    public class ResponseGetListContents : Response
    {
        public List<ContentsDto> listContents { get; set; }

        public ResponseGetListContents()
        {

        }
    }
}
