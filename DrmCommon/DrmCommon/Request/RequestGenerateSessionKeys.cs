﻿using DrmCommon.Dto.SessionKey;
using DrmCommon.Result;

namespace DrmCommon.Request
{
    //Генерация сеансовых ключей 2 этапа
    public class RequestGenerateSessionKeys : Request
    {
        public SessionKeyGenerateDto sessionKeyGenerateDto { get; set; }
        //нужен для обновления ключей
        public string userLogin { get; set; }
        public Operation Operation { get; set; }

        public RequestGenerateSessionKeys()
        {
            this.Operation = Operation.None;
        }
    }
}
