﻿using System.Net.Sockets;

namespace DrmCommon.Request
{
    public enum TypeRequest
    {
        GenerateSessionKeyStep1, //Первый шаг генерации Сеансовых ключей
        GenerateSessionKeyStep2, //Второй шаг генерации Сеансовых ключей
        RequestAuthorization, //Отправка данных для авторизации
        ResultAuthorization, //Результат авторизации

        Registration,
        Logout,

        CheckAccess,
        ChangePassword,

        BuyContent,
        DeleteContent,
        GetListContentsAll,
        GetListUserContents,
        GetOpenContent
    }

    //На сервер
    public class Request
    {
        public TypeRequest typeRequest { get; set; }
        public bool waitResponse { get; set; }
        public Socket socket { get; set; }
        public int IDKey { get; set; }

        public Request()
        {

        }
    }
}
