﻿using DrmCommon.Dto.Users;
using DrmCommon.Result;

namespace DrmCommon.Request
{
    //Авторизация, регистрация, разрешение доступа
    public class RequestAuthorization : Request
    {
        public UserDto CurrentUserDtoEncripted { get; set; }
        public Operation Operation { get; set; }

        public RequestAuthorization()
        {
            this.Operation = Operation.None;
        }
    }
}
