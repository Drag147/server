﻿namespace DrmCommon.Request.RequestContent
{
    public class RequestGetListContents : RequestContent
    { 
        public RequestGetListContents()
        {
            this.typeRequest = TypeRequest.GetListContentsAll;
        }
    }
}
