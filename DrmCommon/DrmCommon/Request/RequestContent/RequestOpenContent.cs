﻿using DrmCommon.Dto.Contents;

namespace DrmCommon.Request.RequestContent
{
    public class RequestOpenContent : RequestContent
    {
        public ContentsDto contentDto { get; set; }

        public RequestOpenContent()
        {

        }
    }
}
