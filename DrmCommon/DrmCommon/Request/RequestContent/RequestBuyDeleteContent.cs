﻿using DrmCommon.Dto.Contents;

namespace DrmCommon.Request.RequestContent
{
    public class RequestBuyDeleteContent : RequestContent
    {
        public ContentsDto contentDto { get; set; }

        public RequestBuyDeleteContent()
        {

        }
    }
}
