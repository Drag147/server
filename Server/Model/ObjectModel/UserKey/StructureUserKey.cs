﻿using DrmCommon.Helper;
using System;

namespace Server.Model.ObjectModel.UserKey
{
    public partial class StructureUserKey
    {
        public const int LENGH_KEY_FOR_BOOK = 256;
        private Random rand;

        public string login { get; set; }
        public BigInteger KeyForContent { get; set; }
        public BigInteger SessionKey { get; set; }
        public DateTime dateTimeSpanForKeyContent { get; set; }

        public bool UpdateKeys = false;

        public StructureUserKey(Random rand)
        {
            this.rand = rand;
            KeyForContent = new BigInteger();
            SessionKey = new BigInteger();
            dateTimeSpanForKeyContent = DateTime.MinValue;
        }

        public void SetSessionKey(BigInteger SessionKey)
        {
            dateTimeSpanForKeyContent = dateTimeSpanForKeyContent == DateTime.MinValue
                ? dateTimeSpanForKeyContent = DateTime.Now
                : dateTimeSpanForKeyContent;
            this.SessionKey = SessionKey;
        }

        public void GenerateKeyForContent()
        {
            KeyForContent = new BigInteger();
            KeyForContent.genRandomBits(LENGH_KEY_FOR_BOOK, rand);
            dateTimeSpanForKeyContent = dateTimeSpanForKeyContent == DateTime.MinValue
                ? dateTimeSpanForKeyContent = DateTime.Now
                : dateTimeSpanForKeyContent;
        }
    }
}
