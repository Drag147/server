﻿namespace Server.Model.ObjectModel
{
    public class ContentsUsers
    {
        public int idContentsUsers { get; set; }
        public int idUser { get; private set; }
        public int idContent { get; private set; }

        public ContentsUsers(int idContentsUsers, int idUser, int idContent)
        {
            this.idContentsUsers = idContentsUsers;
            this.idUser = idUser;
            this.idContent = idContent;
        }

        public ContentsUsers(int idUser, int idContent)
        {
            this.idUser = idUser;
            this.idContent = idContent;
        }
    }
}
