﻿namespace Server.Model.ObjectModel
{
    public class User
    {
        public int id { get; set; }
        public string Login { get; set; }
        public string PasswordHash { get; set; }

        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public string Mail { get; set; }

        public User ()
        {

        }

        public User(string Login, string PasswordHash)
        {
            this.Login = Login;
            this.PasswordHash = PasswordHash;
        }

        public User(string Login, string PasswordHash, string Name, string Surname, string Patronymic, string Mail)
        {
            this.Login = Login;
            this.PasswordHash = PasswordHash;

            this.Name = Name;
            this.Surname = Surname;
            this.Patronymic = Patronymic;
            this.Mail = Mail;
        }
    }
}
