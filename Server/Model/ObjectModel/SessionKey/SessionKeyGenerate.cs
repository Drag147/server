﻿using DrmCommon.Helper;

namespace Server.Model.ObjectModel.ObjectModel
{
    public class SessionKeyGenerate
    {
        public int IDKey { get; set; }
        public BigInteger N { get; set; }
        public BigInteger G { get; set; }
        public BigInteger X { get; set; }
        public BigInteger Y { get; set; }
        public BigInteger Xs { get; set; }
        public BigInteger Key { get; set; }

        public SessionKeyGenerate()
        {
            IDKey = -1;
        }
    }
}
