﻿namespace Server.Model.ObjectModel
{
    public class Content
    {
        public int id { get; set; }
        public string NameContent { get; set; }
        public byte[] ContentData { get; set; }
        public uint Size { get; set; }

        public Content()
        {

        }
    }
}
