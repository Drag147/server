﻿using Server.Model.ObjectModel;

namespace Server.Model.Repository.Specification.Users
{
    class UserSpecificationByLogin : UserSpecification, CommonSpecificationForMySql
    {
        string Login;

        public UserSpecificationByLogin (string Login)
        {
            this.Login = Login;
        }

        public bool specified(User user)
        {
            return user.Login.Equals(this.Login);
        }

        public string toMySqlSpecified()
        {
            return " where `login`='" + this.Login + "'";
        }
    }
}
