﻿using Server.Model.ObjectModel;

namespace Server.Model.Repository.Specification.Users
{
    class UserSpecificationByLoginAndPassword : UserSpecification, CommonSpecificationForMySql
    {
        public string Login { get; set; }
        public string PasswordHash { get; set; }

        public UserSpecificationByLoginAndPassword(string Login, string PasswordHash)
        {
            this.Login = Login;
            this.PasswordHash = PasswordHash;
        }

        public bool specified(User user)
        {
            return user.Login.Equals(this.Login) && user.PasswordHash.Equals(this.PasswordHash);
        }

        public string toMySqlSpecified()
        {
            return " where `login`='" + this.Login + "' and `password`='" + this.PasswordHash + "'";
        }
    }
}
