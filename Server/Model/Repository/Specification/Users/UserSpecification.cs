﻿using Server.Model.ObjectModel;

namespace Server.Model.Repository.Specification.Users
{
    interface UserSpecification : CommonSpecification
    {
        bool specified(User user);
    }
}