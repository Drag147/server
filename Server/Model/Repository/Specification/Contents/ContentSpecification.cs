﻿using Server.Model.ObjectModel;

namespace Server.Model.Repository.Specification.Contents
{
    interface ContentSpecification : CommonSpecification
    {
        bool specified(Content content);
    }
}
