﻿using Server.Model.ObjectModel;

namespace Server.Model.Repository.Specification.Contents
{
    class ContentSpecificationById : ContentSpecification, CommonSpecificationForMySql
    {
        private int id;
        private bool withContent;

        public ContentSpecificationById(int id, bool withContent = false)
        {
            this.id = id;
            this.withContent = withContent;
        }

        public bool specified(Content content)
        {
            return content.id == this.id;
        }

        public string toMySqlSpecified()
        {
            return " where `id_content`='" + this.id + "'";
        }
    }
}
