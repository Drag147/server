﻿using Server.Model.ObjectModel;
using System.Collections.Generic;

namespace Server.Model.Repository.Specification.Contents
{
    class ContentSpecificationByListId : ContentSpecification, CommonSpecificationForMySql
    {
        private readonly List<int> idsForSelect;

        public ContentSpecificationByListId(List<int> idsForSelect)
        {
            this.idsForSelect = idsForSelect;
        }

        public bool specified(Content content)
        {
            return idsForSelect.Contains(content.id);
        }

        public string toMySqlSpecified()
        {
            string resString = "";
            if (idsForSelect.Count > 0)
            {
                resString = "where `id_content`=";
                for (int i = 0; i < idsForSelect.Count; i++)
                {
                    resString += "'" + idsForSelect[i] + "'";

                    if (i+1 != idsForSelect.Count)
                    {
                        resString += " or `id_content`=";
                    }
                }
            }
            return resString;
        }
    }
}
