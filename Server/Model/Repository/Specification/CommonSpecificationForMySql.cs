﻿namespace Server.Model.Repository.Specification
{
    interface CommonSpecificationForMySql
    {
        string toMySqlSpecified();
    }
}
