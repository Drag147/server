﻿namespace Server.Model.Repository.Specification.ContentsUsers
{
    class ContentsUsersSpecificationByIdContent : ContentsUsersSpecification, CommonSpecificationForMySql
    {
        private readonly int idContent;

        public ContentsUsersSpecificationByIdContent(int idContent)
        {
            this.idContent = idContent;
        }

        public bool specified(ObjectModel.ContentsUsers contentsUsers)
        {
            return this.idContent == contentsUsers.idContent;
        }

        public string toMySqlSpecified()
        {
            return "where `id_content`='" + this.idContent + "'";
        }
    }
}
