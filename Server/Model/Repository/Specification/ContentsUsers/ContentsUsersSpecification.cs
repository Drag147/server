﻿namespace Server.Model.Repository.Specification.ContentsUsers
{
    interface ContentsUsersSpecification : CommonSpecification
    {
        bool specified(ObjectModel.ContentsUsers contentsUsers);
    }
}
