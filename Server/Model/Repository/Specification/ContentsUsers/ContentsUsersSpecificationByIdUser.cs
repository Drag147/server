﻿namespace Server.Model.Repository.Specification.ContentsUsers
{
    class ContentsUsersSpecificationByIdUser : ContentsUsersSpecification, CommonSpecificationForMySql
    {
        private readonly int idUser;

        public ContentsUsersSpecificationByIdUser(int idUser)
        {
            this.idUser = idUser;
        }

        public bool specified(ObjectModel.ContentsUsers contentsUsers)
        {
            return this.idUser == contentsUsers.idUser;
        }

        public string toMySqlSpecified()
        {
            return " where `id_user`='" + this.idUser + "'";
        }
    }
}
