﻿namespace Server.Model.Repository.Specification.ContentsUsers
{
    class ContentsUsersSpecificationById : ContentsUsersSpecification, CommonSpecificationForMySql
    {
        private readonly int id;

        public ContentsUsersSpecificationById(int id)
        {
            this.id = id;
        }

        public bool specified(ObjectModel.ContentsUsers contentsUsers)
        {
            return contentsUsers.idContentsUsers == this.id;
        }

        public string toMySqlSpecified()
        {
            return " where `id_contents_users`='" + this.id + "'";
        }
    }
}
