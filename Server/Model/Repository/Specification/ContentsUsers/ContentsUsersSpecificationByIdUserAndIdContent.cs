﻿namespace Server.Model.Repository.Specification.ContentsUsers
{
    public class ContentsUsersSpecificationByIdUserAndIdContent : ContentsUsersSpecification, CommonSpecificationForMySql
    {
        private int idUser;
        private int idContent;

        public ContentsUsersSpecificationByIdUserAndIdContent(int idUser, int idContent)
        {
            this.idUser = idUser;
            this.idContent = idContent;
        }

        public bool specified(ObjectModel.ContentsUsers contentsUsers)
        {
            return contentsUsers.idContent == idContent && contentsUsers.idUser == idUser;
        }

        public string toMySqlSpecified()
        {
            return " where `id_user`='" + idUser + "' and `id_content`='" + idContent + "'"; 
        }
    }
}
