﻿using MySql.Data.MySqlClient;
using NLog;
using Server.ConnectionControl.MySql;
using Server.Model.ObjectModel;
using Server.Model.Repository.Specification;
using Server.Model.Repository.Specification.Contents;
using System;
using System.Collections.Generic;

namespace Server.Model.Repository.Contents
{
    public class ContentRepository : ObjectRepository<Content>
    {
        private readonly static Logger logger = LogManager.GetCurrentClassLogger();

        private readonly MySqlDatabaseConnectionControl mySqlDatabaseConnectionControl =
                                    MySqlDatabaseConnectionControl.GetMySqlDatabaseConnectionControl();

        private readonly string selectContentsSql = Server.Properties.Resources.selectContents;
        private readonly string selectContentsWithoutContentSql = Server.Properties.Resources.selectContentWithoutContent;
        private readonly string insertContentsSql = Server.Properties.Resources.insertContents;
        private readonly string updateContentsSql = Server.Properties.Resources.updateContents;
        private readonly string deleteContentsSql = Server.Properties.Resources.deleteContents;

        private const int BufferSize = 100_000_000;

        public bool create(Content objectModel)
        {
            MySqlConnection connection = mySqlDatabaseConnectionControl.GetDBConnection();
            try
            {
                connection.Open();

                /*
                 * INSERT INTO
	                    `@DbName`.`@TableNameContents`
	                    (`content_name`, `content`, `size`)
                   VALUES
	                    (@content_name, @content, @size);
                 */
                string sqlQuery = this.insertContentsSql.Replace("@DbName", Properties.Settings.Default.databaseName).
                  Replace("@TableNameContents", Properties.Resources.TableNameContents);

                MySqlCommand mySqlCommand = new MySqlCommand(sqlQuery, connection);
                mySqlCommand.Parameters.Add("@content_name", MySqlDbType.VarChar).Value = objectModel.NameContent;

                logger.Debug(objectModel.ContentData.Length / 1024 / 1024 + " мБайт");

                mySqlCommand.Parameters.Add("@content", MySqlDbType.LongBlob).Value = objectModel.ContentData;
                mySqlCommand.Parameters.Add("@size", MySqlDbType.UInt32).Value = objectModel.ContentData.Length;

                int rowCount = mySqlCommand.ExecuteNonQuery();

                logger.Debug(sqlQuery);

                return rowCount == 1;
            }
            catch (Exception exept)
            {
                logger.Error(exept);
            }
            finally
            {
                mySqlDatabaseConnectionControl.CloseConnection();
            }
            return false;
        }

        public bool remove(Content objectModel)
        {
            MySqlConnection connection = mySqlDatabaseConnectionControl.GetDBConnection();
            try
            {
                connection.Open();

                //delete from `@DbName`.`@TableNameContents` WHERE (`id_content` = @id_content);
                string sqlQuery = this.deleteContentsSql.Replace("@DbName", Server.Properties.Settings.Default.databaseName).
                  Replace("@TableNameContents", Server.Properties.Resources.TableNameContents);
                MySqlCommand mySqlCommand = new MySqlCommand(sqlQuery, connection);
                mySqlCommand.Parameters.Add("@id_content", MySqlDbType.Int32).Value = objectModel.id;

                int rowCount = mySqlCommand.ExecuteNonQuery();

                logger.Debug(sqlQuery);

                return rowCount == 1;
            }
            catch (Exception exept)
            {
                logger.Error(exept);
            }
            finally
            {
                mySqlDatabaseConnectionControl.CloseConnection();
            }
            return false;
        }

        public Content selectOne(CommonSpecification modelSpecificator)
        {
            List<Content> contents = select(modelSpecificator);

            if (contents == null || contents.Count == 0)
            {
                throw new ArgumentNullException("Не удалось найти контент в базе данных.");
            }
            if (contents.Count > 1)
            {
                throw new ArgumentException("Контента с данным id более 1.");
            }

            return contents[0];
        }

        public List<Content> select(CommonSpecification modelSpecificator)
        {
            /*
             fs = new FileStream(@"C:\newfile.png", FileMode.OpenOrCreate, FileAccess.Write);
             fs.Write(rawData, 0, FileSize);
             fs.Close();
             */

            List<Content> resultList = null;
            MySqlConnection connection = mySqlDatabaseConnectionControl.GetDBConnection();
            try
            {
                connection.Open();
                /*
                 *С контентом
                 * select
	                    `@DbName`.`@TableNameContents`.`id_content`,
	                    `@DbName`.`@TableNameContents`.`content_name`,
	                    `@DbName`.`@TableNameContents`.`content`,
	                    `@DbName`.`@TableNameContents`.`size`
                   from `@DbName`.`@TableNameContents`
                 */
                /*
                 *Без контента
                 * select
	                    `@DbName`.`@TableNameContents`.`id_content`,
	                    `@DbName`.`@TableNameContents`.`content_name`,
	                    `@DbName`.`@TableNameContents`.`size`
                   from `@DbName`.`@TableNameContents`
                 */
                bool withContent = false;

                if (modelSpecificator is ContentSpecificationById)
                {
                    withContent = true;
                }

                string sqlQuery = sqlQuery = this.selectContentsWithoutContentSql.Replace("@DbName", Server.Properties.Settings.Default.databaseName).
                    Replace("@TableNameContents", Server.Properties.Resources.TableNameContents);

                if (withContent)
                {
                    sqlQuery = this.selectContentsSql.Replace("@DbName", Server.Properties.Settings.Default.databaseName).
                    Replace("@TableNameContents", Server.Properties.Resources.TableNameContents);
                }

                if (modelSpecificator is CommonSpecificationForMySql)
                {
                    sqlQuery += ((CommonSpecificationForMySql)modelSpecificator).toMySqlSpecified();
                }

                logger.Debug(sqlQuery);

                MySqlCommand mySqlCommand = new MySqlCommand(sqlQuery, connection);

                using (MySqlDataReader reader = mySqlCommand.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        resultList = new List<Content>();
                        while (reader.Read())
                        {
                            int id_content = reader.GetInt32(reader.GetOrdinal("id_content"));
                            string content_name = reader.GetString(reader.GetOrdinal("content_name"));
                            uint size =  reader.GetUInt32(reader.GetOrdinal("size"));

                            Content content = new Content
                            {
                                id = id_content,
                                NameContent = content_name,
                                Size = size,
                                
                            };

                            if (withContent)
                            {
                                content.ContentData = new byte[size];
                                //byte[] buffer = new byte[BufferSize];
                                long lengReadedData = 0;
                                int indexFieldContent = reader.GetOrdinal("content");
                                while (lengReadedData < size)
                                {
                                    long readed = reader.GetBytes(
                                        indexFieldContent,
                                        lengReadedData,
                                        content.ContentData,
                                        (int)lengReadedData,
                                        (int)size);
                                    lengReadedData += readed;
                                }
                            }

                            resultList.Add(content);
                        }
                    }
                    else
                    {
                        resultList = new List<Content>();
                    }
                }
            }
            catch (Exception exept)
            {
                logger.Error(exept);
            }
            finally
            {
                mySqlDatabaseConnectionControl.CloseConnection();
            }
            return resultList;
        }

        public bool update(Content newObject)
        {
            MySqlConnection connection = mySqlDatabaseConnectionControl.GetDBConnection();
            try
            {
                connection.Open();

                /*
                 * update
	                    `@DbName`.`@TableNameContents`
	               SET
	                    `content_name`=@content_name,
	                    `content`=@content,
                        `size`=@size
	               where (`id_content` = @id_content)
                 * */
                string sqlQuery = this.updateContentsSql.Replace("@DbName", Properties.Settings.Default.databaseName).
                   Replace("@TableNameContents", Properties.Resources.TableNameContents);
                MySqlCommand mySqlCommand = new MySqlCommand(sqlQuery, connection);
                mySqlCommand.Parameters.Add("@content_name", MySqlDbType.VarChar).Value = newObject.NameContent;

                mySqlCommand.Parameters.Add("@content", MySqlDbType.LongBlob).Value = newObject.ContentData;
                mySqlCommand.Parameters.Add("@size", MySqlDbType.UInt32).Value = newObject.ContentData.Length;

                int rowCount = mySqlCommand.ExecuteNonQuery();

                logger.Debug(sqlQuery);

                return rowCount == 1;
            }
            catch (Exception exept)
            {
                logger.Error(exept);
            }
            finally
            {
                mySqlDatabaseConnectionControl.CloseConnection();
            }
            return false;
        }
    }
}
