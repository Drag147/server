﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using NLog;
using Server.ConnectionControl.MySql;
using Server.Model.Repository.Specification;

namespace Server.Model.Repository.ContentsUsers
{
    public class ContentsUsersRepository : ObjectRepository<ObjectModel.ContentsUsers>
    {
        private readonly static Logger logger = LogManager.GetCurrentClassLogger();

        private readonly MySqlDatabaseConnectionControl mySqlDatabaseConnectionControl =
                                    MySqlDatabaseConnectionControl.GetMySqlDatabaseConnectionControl();

        private readonly string selectContentsUsersSql = Server.Properties.Resources.selectContentsUsers;
        private readonly string insertContentsUsersSql = Server.Properties.Resources.insertContentsUsers;
        private readonly string updateContentsUsersSql = Server.Properties.Resources.updateContentsUsers;
        private readonly string deleteContentsUsersSql = Server.Properties.Resources.deleteContentsUsers;
        private readonly string deleteContentsUsersSqlByUserIdAndContentId = Server.Properties.Resources.deleteContentsUsersByUserIdAndcontentId;

        public bool create(ObjectModel.ContentsUsers objectModel)
        {
            MySqlConnection connection = mySqlDatabaseConnectionControl.GetDBConnection();
            try
            {
                connection.Open();

                /*
                 *INSERT INTO
	                    `@DbName`.`@TableNameContentsUsers`
	                    (`id_user`, `id_content`)
                  VALUES
	                    (@id_user, @id_content);
                 */
                string sqlQuery = this.insertContentsUsersSql.Replace("@DbName", Server.Properties.Settings.Default.databaseName).
                  Replace("@TableNameContentsUsers", Server.Properties.Resources.TableNameContentsUsers);

                MySqlCommand mySqlCommand = new MySqlCommand(sqlQuery, connection);
                mySqlCommand.Parameters.Add("@id_user", MySqlDbType.Int32).Value = objectModel.idUser;
                mySqlCommand.Parameters.Add("@id_content", MySqlDbType.Int32).Value = objectModel.idContent;

                int rowCount = mySqlCommand.ExecuteNonQuery();

                logger.Debug(sqlQuery);

                return rowCount == 1;
            }
            catch (Exception exept)
            {
                logger.Error(exept);
            }
            finally
            {
                mySqlDatabaseConnectionControl.CloseConnection();
            }
            return false;
        }

        public bool remove(ObjectModel.ContentsUsers objectModel)
        {
            MySqlConnection connection = mySqlDatabaseConnectionControl.GetDBConnection();
            try
            {
                connection.Open();

                //delete from `@DbName`.`@TableNameContentsUsers` WHERE (`id_contents_users` = @id_contents_users);
                //ИЛИ delete from `@DbName`.`@TableNameContentsUsers` WHERE (`id_user` = @id_user) and (`id_content` = @id_content);
                string sqlQuery = this.deleteContentsUsersSqlByUserIdAndContentId.Replace("@DbName", Server.Properties.Settings.Default.databaseName).
                    Replace("@TableNameContentsUsers", Server.Properties.Resources.TableNameContentsUsers);
                MySqlCommand mySqlCommand = new MySqlCommand(sqlQuery, connection);
                //mySqlCommand.Parameters.Add("@id_contents_users", MySqlDbType.Int32).Value = objectModel.idContentsUsers;
                mySqlCommand.Parameters.Add("@id_user", MySqlDbType.Int32).Value = objectModel.idUser;
                mySqlCommand.Parameters.Add("@id_content", MySqlDbType.Int32).Value = objectModel.idContent;

                int rowCount = mySqlCommand.ExecuteNonQuery();

                logger.Debug(sqlQuery);

                return rowCount == 1;
            }
            catch (Exception exept)
            {
                logger.Error(exept);
            }
            finally
            {
                mySqlDatabaseConnectionControl.CloseConnection();
            }
            return false;
        }

        public ObjectModel.ContentsUsers selectOne(CommonSpecification modelSpecificator)
        {
            List<ObjectModel.ContentsUsers> contentsUsers = select(modelSpecificator);

            if (contentsUsers == null || contentsUsers.Count == 0)
            {
                throw new ArgumentNullException("Не удалось найти связку пользователь -> контент. Возможно у вас не куплен этот контент");
            }
            if (contentsUsers.Count > 1)
            {
                throw new ArgumentException("Связок пользователь -> контент более 1, что указывает на ошибку в базе.");
            }

            return contentsUsers[0];
        }

        public List<ObjectModel.ContentsUsers> select(CommonSpecification modelSpecificator)
        {
            List<ObjectModel.ContentsUsers> resultList = null;
            MySqlConnection connection = mySqlDatabaseConnectionControl.GetDBConnection();
            try
            {
                connection.Open();
                /*
                 * 
                 * select
	                    `@DbName`.`@TableNameContentsUsers`.`id_contents_users`,
	                    `@DbName`.`@TableNameContentsUsers`.`id_content`,
	                    `@DbName`.`@TableNameContentsUsers`.`id_user`,
                   from `@DbName`.`@TableNameContentsUsers`
                 */
                string sqlQuery = this.selectContentsUsersSql.Replace("@DbName", Server.Properties.Settings.Default.databaseName).
                    Replace("@TableNameContentsUsers", Server.Properties.Resources.TableNameContentsUsers);
                if (modelSpecificator is CommonSpecificationForMySql)
                {
                    sqlQuery += ((CommonSpecificationForMySql)modelSpecificator).toMySqlSpecified();
                }

                logger.Debug(sqlQuery);

                MySqlCommand mySqlCommand = new MySqlCommand(sqlQuery, connection);

                using (MySqlDataReader reader = mySqlCommand.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        resultList = new List<ObjectModel.ContentsUsers>();
                        while (reader.Read())
                        {
                            int id_contents_users = reader.GetInt32(reader.GetOrdinal("id_contents_users"));
                            int id_content = reader.GetInt32(reader.GetOrdinal("id_content"));
                            int id_user = reader.GetInt32(reader.GetOrdinal("id_user"));

                            ObjectModel.ContentsUsers ContentsUsers = new ObjectModel.ContentsUsers(id_contents_users, id_user, id_content);
                            resultList.Add(ContentsUsers);
                        }
                    }
                    else
                    {
                        resultList = new List<ObjectModel.ContentsUsers>();
                    }
                }
            }
            catch (Exception exept)
            {
                logger.Error(exept);
            }
            finally
            {
                mySqlDatabaseConnectionControl.CloseConnection();
            }
            return resultList;
        }

        public bool update(ObjectModel.ContentsUsers newObject)
        {
            MySqlConnection connection = mySqlDatabaseConnectionControl.GetDBConnection();
            try
            {
                connection.Open();

                /*
                 * update
	                    `@DbName`.`@TableNameContentsUsers`
	               SET
	                    `id_content`=@id_content,
	                    `id_user`=@id_user
	               where (`id_contents_users` = @id_contents_users)
                 * */
                string sqlQuery = this.updateContentsUsersSql.Replace("@DbName", Server.Properties.Settings.Default.databaseName).
                   Replace("@TableNameContentsUsers", Server.Properties.Resources.TableNameContentsUsers);
                MySqlCommand mySqlCommand = new MySqlCommand(sqlQuery, connection);
                mySqlCommand.Parameters.Add("@id_content", MySqlDbType.VarChar).Value = newObject.idContent;
                mySqlCommand.Parameters.Add("@id_user", MySqlDbType.LongBlob).Value = newObject.idUser;
                mySqlCommand.Parameters.Add("@id_contents_users", MySqlDbType.UInt32).Value = newObject.idContentsUsers;

                int rowCount = mySqlCommand.ExecuteNonQuery();

                logger.Debug(sqlQuery);

                return rowCount == 1;
            }
            catch (Exception exept)
            {
                logger.Error(exept);
            }
            finally
            {
                mySqlDatabaseConnectionControl.CloseConnection();
            }
            return false;
        }
    }
}