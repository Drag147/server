﻿using MySql.Data.MySqlClient;
using Server.ConnectionControl.MySql;
using System;
using System.Collections.Generic;
using NLog;
using System.Data.Common;
using Server.Model.ObjectModel;
using Server.Model.Repository.Specification;

namespace Server.Model.Repository.Users
{
    //класс для работы с базой
    public class UserRepository : ObjectRepository<User>
    {
        private readonly static Logger logger = LogManager.GetCurrentClassLogger();

        private readonly MySqlDatabaseConnectionControl mySqlDatabaseConnectionControl =
                                    MySqlDatabaseConnectionControl.GetMySqlDatabaseConnectionControl();

        private readonly string selectUserSql = Server.Properties.Resources.selectUsers;
        private readonly string insertUserSql = Server.Properties.Resources.insertUsers;
        private readonly string updateUserSql = Server.Properties.Resources.updateUsers;
        private readonly string deleteUserSql = Server.Properties.Resources.deleteUsers;

        public UserRepository()
        {

        }

        public bool create(User objectModel)
        {
            MySqlConnection connection = mySqlDatabaseConnectionControl.GetDBConnection();
            try
            {
                connection.Open();

                /*
                 INSERT INTO
	                `@DbName`.`@TableNameUsers`
	                (`login`, `password`, `name`, `surname`, `patronymic`, `mail`)
                VALUES
	                ('@login', '@password', '@name', '@surname', '@patronymic', '@mail');
                 */
                string sqlQuery = this.insertUserSql.Replace("@DbName", Server.Properties.Settings.Default.databaseName).
                  Replace("@TableNameUsers", Server.Properties.Resources.TableNameUsers);
                MySqlCommand mySqlCommand = new MySqlCommand(sqlQuery, connection);
                mySqlCommand.Parameters.Add("@login", MySqlDbType.VarChar).Value = objectModel.Login;
                mySqlCommand.Parameters.Add("@password", MySqlDbType.VarChar).Value = objectModel.PasswordHash;
                mySqlCommand.Parameters.Add("@name", MySqlDbType.VarChar).Value = objectModel.Name;
                mySqlCommand.Parameters.Add("@surname", MySqlDbType.VarChar).Value = objectModel.Surname;
                mySqlCommand.Parameters.Add("@patronymic", MySqlDbType.VarChar).Value = objectModel.Patronymic;
                mySqlCommand.Parameters.Add("@mail", MySqlDbType.VarChar).Value = objectModel.Mail;

                int rowCount = mySqlCommand.ExecuteNonQuery();

                logger.Debug(sqlQuery);

                return rowCount == 1;
            }
            catch (Exception exept)
            {
                logger.Error(exept);
            }
            finally
            {
                mySqlDatabaseConnectionControl.CloseConnection();
            }
            return false;
        }

        public bool remove(User objectModel)
        {
            MySqlConnection connection = mySqlDatabaseConnectionControl.GetDBConnection();
            try
            {
                connection.Open();

                //delete from `@DbName`.`@TableNameUsers` WHERE (`login` = @login);
                string sqlQuery = this.deleteUserSql.Replace("@DbName", Server.Properties.Settings.Default.databaseName).
                  Replace("@TableNameUsers", Server.Properties.Resources.TableNameUsers);
                MySqlCommand mySqlCommand = new MySqlCommand(sqlQuery, connection);
                mySqlCommand.Parameters.Add("@login", MySqlDbType.VarChar).Value = objectModel.Login;

                int rowCount = mySqlCommand.ExecuteNonQuery();

                logger.Debug(sqlQuery);

                return rowCount == 1;
            }
            catch (Exception exept)
            {
                logger.Error(exept);
            }
            finally
            {
                mySqlDatabaseConnectionControl.CloseConnection();
            }
            return false;
        }

        public User selectOne(CommonSpecification modelSpecificator)
        {
            List<User> users = select(modelSpecificator);

            if (users == null || users.Count == 0)
            {
                throw new ArgumentNullException("Такого пользователя не найдено");
            }
            if (users.Count > 1)
            {
                throw new ArgumentException("Найдено более 1 пользователя");
            }

            return users[0];
        }

        public List<User> select(CommonSpecification modelSpecificator)
        {
            List<User> resultList = null;
            MySqlConnection connection = mySqlDatabaseConnectionControl.GetDBConnection();
            try
            {
                connection.Open();
                /*
                 * 
                 * select
	                    `@DbName`.`@TableNameUsers`.`id`,
	                    `@DbName`.`@TableNameUsers`.`login`,
	                    `@DbName`.`@TableNameUsers`.`password`,
	                    `@DbName`.`@TableNameUsers`.`name`,
	                    `@DbName`.`@TableNameUsers`.`surname`,
	                    `@DbName`.`@TableNameUsers`.`patronymic`,
	                    `@DbName`.`@TableNameUsers`.`mail`,
                  from `@DbName`.`@TableNameUsers`
                 */
                string sqlQuery = this.selectUserSql.Replace("@DbName", Server.Properties.Settings.Default.databaseName).
                    Replace("@TableNameUsers", Server.Properties.Resources.TableNameUsers);
                if (modelSpecificator is CommonSpecificationForMySql)
                {
                    sqlQuery += ((CommonSpecificationForMySql)modelSpecificator).toMySqlSpecified();
                }

                logger.Debug(sqlQuery);

                MySqlCommand mySqlCommand = new MySqlCommand(sqlQuery, connection);

                using (DbDataReader reader = mySqlCommand.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        resultList = new List<User>();
                        while (reader.Read())
                        {
                            int id = reader.GetInt32(reader.GetOrdinal("id"));
                            string login = reader.GetString(reader.GetOrdinal("login"));
                            string password = reader.GetString(reader.GetOrdinal("password"));
                            string name = null;
                            if (!reader.IsDBNull(reader.GetOrdinal("name")))
                            {
                                name = reader.GetString(reader.GetOrdinal("name"));
                            }
                            string surname = null;
                            if (!reader.IsDBNull(reader.GetOrdinal("surname")))
                            {
                                surname = reader.GetString(reader.GetOrdinal("surname"));
                            }
                            string patronymic = null;
                            if (!reader.IsDBNull(reader.GetOrdinal("patronymic")))
                            {
                                patronymic = reader.GetString(reader.GetOrdinal("patronymic"));
                            }
                            string mail = null;
                            if (!reader.IsDBNull(reader.GetOrdinal("mail")))
                            {
                                mail = reader.GetString(reader.GetOrdinal("mail"));
                            }

                            User user = new User
                            {
                                id = id,
                                Login = login,
                                PasswordHash = password,
                                Name = name,
                                Surname = surname,
                                Patronymic = patronymic,
                                Mail = mail
                            };

                            resultList.Add(user);
                        }
                    }
                    else
                    {
                        resultList = new List<User>();
                    }
                }
            }
            catch (Exception exept)
            {
                logger.Error(exept);
            }
            finally
            {
                mySqlDatabaseConnectionControl.CloseConnection();
            }
            return resultList;
        }

        public bool update(User newObject)
        {
            MySqlConnection connection = mySqlDatabaseConnectionControl.GetDBConnection();
            try
            {
                connection.Open();

                /*
                 * update
	                    `@DbName`.`@TableNameUsers`
                   SET
	                    `password`=@password,
	                    `name`=@name,
	                    `surnname`=@surname,
	                    `patronymic`=@patronymic,
	                    `mail`=@mail
                   where (`login` = @login)
                 * */
                string sqlQuery = this.updateUserSql.Replace("@DbName", Server.Properties.Settings.Default.databaseName).
                   Replace("@TableNameUsers", Server.Properties.Resources.TableNameUsers);
                MySqlCommand mySqlCommand = new MySqlCommand(sqlQuery, connection);
                mySqlCommand.Parameters.Add("@password", MySqlDbType.VarChar).Value = newObject.PasswordHash;
                mySqlCommand.Parameters.Add("@name", MySqlDbType.VarChar).Value = newObject.Name;
                mySqlCommand.Parameters.Add("@surname", MySqlDbType.VarChar).Value = newObject.Surname;
                mySqlCommand.Parameters.Add("@patronymic", MySqlDbType.VarChar).Value = newObject.Patronymic;
                mySqlCommand.Parameters.Add("@mail", MySqlDbType.VarChar).Value = newObject.Mail;
                mySqlCommand.Parameters.Add("@login", MySqlDbType.VarChar).Value = newObject.Login;

                int rowCount = mySqlCommand.ExecuteNonQuery();

                logger.Debug(sqlQuery);

                return rowCount == 1;
            }
            catch (Exception exept)
            {
                logger.Error(exept);
            }
            finally
            {
                mySqlDatabaseConnectionControl.CloseConnection();
            }
            return false;
        }
    }
}