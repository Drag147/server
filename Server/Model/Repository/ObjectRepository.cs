﻿using Server.Model.Repository.Specification;
using System.Collections.Generic;

namespace Server.Model.Repository
{
    interface ObjectRepository<T>
    {
        T selectOne(CommonSpecification modelSpecificator);

        List<T> select(CommonSpecification modelSpecificator);

        bool create(T objectModel);

        bool update(T newObject);

        bool remove(T objectModel);
    }
}
