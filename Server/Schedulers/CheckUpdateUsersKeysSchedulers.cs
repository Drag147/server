﻿using System.Collections.Generic;
using Quartz;
using Quartz.Impl;
using Server.Jobs;
using Server.Model.ObjectModel.UserKey;

namespace Server.Schedulers
{
    public class CheckUpdateUsersKeysSchedulers
    {
        public static async void Start(Dictionary<int, StructureUserKey> UsersKeys, int AfterCheckMinutes, int TimeToUpdateKeys)
        {
            IScheduler scheduler = await StdSchedulerFactory.GetDefaultScheduler();
            await scheduler.Start();

            IJobDetail job = JobBuilder.Create<JobCheckUpdateUsersKeys>().Build();
            job.JobDataMap.Add("UsersKeys", UsersKeys);
            job.JobDataMap.Add("IntervalCheck", TimeToUpdateKeys);

            ITrigger trigger = TriggerBuilder.Create()  // создаем триггер
                .WithIdentity("CheckUpdateKeysUsers", "CheckKeys")     // идентифицируем триггер с именем и группой
                .StartNow()                            // запуск сразу после начала выполнения
                .WithSimpleSchedule(x => x            // настраиваем выполнение действия
                    .WithIntervalInMinutes(AfterCheckMinutes)       // через 1 час
                    .RepeatForever())                   // бесконечное повторение
                .Build();                               // создаем триггер

            await scheduler.ScheduleJob(job, trigger);        // начинаем выполнение работы
        }
    }
}
