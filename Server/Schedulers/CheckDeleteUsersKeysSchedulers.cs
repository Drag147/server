﻿using System.Collections.Generic;
using Quartz;
using Quartz.Impl;
using Server.Jobs;
using Server.Model.ObjectModel.UserKey;

namespace Server.Schedulers
{
    public class CheckDeleteUsersKeysSchedulers
    {
        public static async void Start(Dictionary<int, StructureUserKey> UsersKeys, int AfterCheckMinutes, int TimeToDeleteKeys)
        {
            IScheduler scheduler = await StdSchedulerFactory.GetDefaultScheduler();
            await scheduler.Start();

            IJobDetail job = JobBuilder.Create<JobCheckDeleteUsersKeys>().Build();
            job.JobDataMap.Add("UsersKeys", UsersKeys);
            job.JobDataMap.Add("IntervalCheck", TimeToDeleteKeys);

            ITrigger trigger = TriggerBuilder.Create()  // создаем триггер
                .WithIdentity("CheckDeleteKeysUsers", "CheckKeys")     // идентифицируем триггер с именем и группой
                .StartNow()                            // запуск сразу после начала выполнения
                .WithSimpleSchedule(x => x            // настраиваем выполнение действия
                    .WithIntervalInMinutes(AfterCheckMinutes)        // через 1 час
                    .RepeatForever())                   // бесконечное повторение
                .Build();                               // создаем триггер

            await scheduler.ScheduleJob(job, trigger);        // начинаем выполнение работы
        }
    }
}
