﻿using System.Collections.Generic;
using DrmCommon.Dto;
using DrmCommon.Dto.Users;
using Server.Model.ObjectModel;
using Server.Services.Crypto.Kuznechik;

namespace Server.Assemblers.UsersAssembler
{
    class UserAssembler : Assembler<User>
    {
        public UserAssembler(KuznechikCryptoService kuznechikCryptoService)
        {
            this.kuznechikCryptoService = kuznechikCryptoService;
        }

        public override User FromDto(ObjectDto toObject, 
            KeyValuePair<int, string> IDKeyAndLogin = default(KeyValuePair<int, string>))
        {
            User user = null;
            if (toObject is UserDto userDto)
            {
                user = Decrypt(userDto, IDKeyAndLogin);
            }
            return user;
        }

        public override ObjectDto ToDto(User objectModel, 
            KeyValuePair<int, string> IDKeyAndLogin = default(KeyValuePair<int, string>))
        {
            UserDto UserDto = new UserDto();
            UserDto.login = objectModel.Login;
            UserDto.passwordHash = objectModel.PasswordHash;
            UserDto.name = objectModel.Name;
            UserDto.surname = objectModel.Surname;
            UserDto.patronymic = objectModel.Patronymic;
            UserDto.mail = objectModel.Mail;
            return UserDto;
        }

        private User Decrypt(UserDto userDto, KeyValuePair<int, string> IDKeyAndLogin)
        {
            User user = new User();
            user.Login = DecryptString(userDto.login, IDKeyAndLogin);
            user.PasswordHash = DecryptString(userDto.passwordHash, IDKeyAndLogin);
            user.Name = userDto.name == null ? null : DecryptString(userDto.name, IDKeyAndLogin);
            user.Surname = userDto.surname == null ? null : DecryptString(userDto.surname, IDKeyAndLogin);
            user.Patronymic = userDto.patronymic == null ? null : DecryptString(userDto.patronymic, IDKeyAndLogin);
            user.Mail = userDto.mail == null ? null : DecryptString(userDto.mail, IDKeyAndLogin);
            return user;
        }

        private string DecryptString(string EncriptedString, KeyValuePair<int, string> IDKeyAndLogin)
        {
            string DecryptedData = kuznechikCryptoService.Decrypt(
                EncriptedString, 
                kuznechikCryptoService.GetUserKey(IDKeyAndLogin, Services.Crypto.CryptoService.TypeKey.SessionKey));
            return DecryptedData;
        }
    }
}
