﻿using DrmCommon.Dto;
using Server.Services.Crypto.Kuznechik;
using System.Collections.Generic;

namespace Server.Assemblers
{
    public abstract class Assembler<T>
    {
        protected KuznechikCryptoService kuznechikCryptoService;

        public abstract ObjectDto ToDto(T objectModel, 
            KeyValuePair<int, string> IDKeyAndLogin = new KeyValuePair<int, string>());

        public abstract T FromDto(ObjectDto toObject, 
            KeyValuePair<int, string> IDKeyAndLogin = new KeyValuePair<int, string>());
    }
}