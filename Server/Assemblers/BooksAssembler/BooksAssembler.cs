﻿using DrmCommon.Dto;
using DrmCommon.Dto.Contents;
using Server.Model.ObjectModel;
using Server.Services.Crypto.Kuznechik;
using System.Collections.Generic;

namespace Server.Assemblers.ContentsAssembler
{
    class ContentsAssembler : Assembler<Content>
    {
        public ContentsAssembler(KuznechikCryptoService kuznechikCryptoService)
        {
            this.kuznechikCryptoService = kuznechikCryptoService;
        }

        public override Content FromDto(ObjectDto toObject, KeyValuePair<int, string> IDKeyAndLogin = new KeyValuePair<int, string>())
        {
            Content content = null;
            if (toObject is ContentsDto)
            {
                ContentsDto contentsDto = (ContentsDto)toObject;
                content = new Content()
                {
                    id = contentsDto.id,
                    NameContent = contentsDto.NameContent,
                    Size = contentsDto.sizeContent
                };
            }
            return content;
        }

        public override ObjectDto ToDto(Content objectModel, KeyValuePair<int, string> IDKeyAndLogin = new KeyValuePair<int, string>())
        {
            ContentsDto contentsDto = new ContentsDto()
            {
                id = objectModel.id,
                NameContent = objectModel.NameContent,
                sizeContent = objectModel.Size
            };

            if (objectModel.ContentData != null)
            {
                contentsDto.ContentData = kuznechikCryptoService.EncryptContent(
                        objectModel.ContentData,
                        kuznechikCryptoService.GetUserKey(IDKeyAndLogin, Services.Crypto.CryptoService.TypeKey.ContentKey)
                    );
            }
            return contentsDto;
        }
    }
}
