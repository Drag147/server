﻿using DrmCommon.Dto;
using DrmCommon.Dto.ContentsUsers;
using Server.Model.ObjectModel;
using System.Collections.Generic;

namespace Server.Assemblers.ContentsUsersAssembler
{
    class ContentsUsersAssembler : Assembler<ContentsUsers>
    {
        public override ContentsUsers FromDto(ObjectDto toObject, KeyValuePair<int, string> IDKeyAndLogin = new KeyValuePair<int, string>())
        {
            ContentsUsers contentsUsers = null;
            if (toObject is ContentsUsersDto contentsUsersDto)
            {
                contentsUsers = new ContentsUsers(contentsUsersDto.idContentsUsers, contentsUsersDto.idUser, contentsUsers.idContent);
            }
            return contentsUsers;
        }

        public override ObjectDto ToDto(ContentsUsers objectModel, KeyValuePair<int, string> IDKeyAndLogin = new KeyValuePair<int, string>())
        {
            ContentsUsersDto contentsUsersDto = new ContentsUsersDto()
            {
                idContentsUsers = objectModel.idContentsUsers,
                idUser = objectModel.idUser,
                idContent = objectModel.idContent
            };

            return contentsUsersDto;
        }
    }
}
