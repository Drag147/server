﻿using System.Collections.Generic;
using DrmCommon.Dto;
using DrmCommon.Dto.SessionKey;
using Server.Model.ObjectModel.ObjectModel;

namespace Server.Assemblers.SessionKeyGenerateAssembler
{
    class SessionKeyGenerateAssembler : Assembler<SessionKeyGenerate>
    {
        public override SessionKeyGenerate FromDto(ObjectDto toObject, KeyValuePair<int, string> IDKeyAndLogin = new KeyValuePair<int, string>())
        {
            SessionKeyGenerate sessionKeyForGenerate = null;
            if (toObject is SessionKeyGenerateDto sessionKeyGenerateDto)
            {
                sessionKeyForGenerate = new SessionKeyGenerate()
                {
                    IDKey = sessionKeyGenerateDto.IDKey,
                    N = sessionKeyGenerateDto.N,
                    G = sessionKeyGenerateDto.G,
                    X = sessionKeyGenerateDto.X,
                    Y = sessionKeyGenerateDto.Y
                };
            }
            return sessionKeyForGenerate;
        }

        public override ObjectDto ToDto(SessionKeyGenerate objectModel, KeyValuePair<int, string> IDKeyAndLogin = new KeyValuePair<int, string>())
        {
            SessionKeyGenerateDto keyGenerateDto = new SessionKeyGenerateDto()
            {
                IDKey = objectModel.IDKey,
                N = objectModel.N,
                G = objectModel.G,
                X = objectModel.X,
                Y = objectModel.Y
            };
            return keyGenerateDto;
        }
    }
}
