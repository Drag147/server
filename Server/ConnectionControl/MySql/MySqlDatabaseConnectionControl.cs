﻿using MySql.Data.MySqlClient;
using System;
using System.Data.Common;

namespace Server.ConnectionControl.MySql
{
    class MySqlDatabaseConnectionControl : DataBaseConnectionControl
    {
        private static MySqlDatabaseConnectionControl mySqlDatabaseConnectionControl = null;

        private MySqlConnection connection = null;
        private string hostDb;
        private string databaseName;
        private int port;
        private string username;
        private string password;

        private MySqlDatabaseConnectionControl()
        {
            this.hostDb = Server.Properties.Settings.Default.hostDb;
            this.databaseName = Server.Properties.Settings.Default.databaseName;
            this.port = Server.Properties.Settings.Default.port;
            this.username = Server.Properties.Settings.Default.username;
            this.password = Server.Properties.Settings.Default.password;
        }

        public static MySqlDatabaseConnectionControl GetMySqlDatabaseConnectionControl()
        {
            if (mySqlDatabaseConnectionControl == null)
            {
                mySqlDatabaseConnectionControl = new MySqlDatabaseConnectionControl();
            }
            return mySqlDatabaseConnectionControl;
        }

        public MySqlConnection GetDBConnection()
        {
            if (this.connection == null)
            {
                // Connection String.
                String connString = "Server=" + this.hostDb + ";Database=" + this.databaseName
                    + ";port=" + this.port + ";User Id=" + this.username + ";password=" + this.password;

                this.connection = new MySqlConnection(connString);
            }

            return this.connection;
        }

        public bool CloseConnection()
        {
            if (this.connection != null && this.connection.State == System.Data.ConnectionState.Open)
            {
                this.connection.Close();
                this.connection.Dispose();
                this.connection = null;
                return true;
            }
            return false;
        }
    }
}
