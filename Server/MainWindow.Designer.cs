﻿namespace Server
{
    partial class MainWindow
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxPort = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxIP = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxSettingsServer = new System.Windows.Forms.GroupBox();
            this.buttonStartServer = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelServer = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusServerLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.buttonSelectPdf = new System.Windows.Forms.Button();
            this.openFileDialogPdf = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialogTiff = new System.Windows.Forms.SaveFileDialog();
            this.textBoxPdfPath = new System.Windows.Forms.TextBox();
            this.textBoxTiffPath = new System.Windows.Forms.TextBox();
            this.buttonUploadContent = new System.Windows.Forms.Button();
            this.buttonUploadTiff = new System.Windows.Forms.Button();
            this.openFileDialogContent = new System.Windows.Forms.OpenFileDialog();
            this.textBoxNameContent = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBoxUploadContent = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDownDpi = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxPort)).BeginInit();
            this.groupBoxSettingsServer.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBoxUploadContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDpi)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxPort
            // 
            this.textBoxPort.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxPort.Location = new System.Drawing.Point(462, 28);
            this.textBoxPort.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.textBoxPort.Minimum = new decimal(new int[] {
            49152,
            0,
            0,
            0});
            this.textBoxPort.Name = "textBoxPort";
            this.textBoxPort.Size = new System.Drawing.Size(84, 26);
            this.textBoxPort.TabIndex = 17;
            this.textBoxPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxPort.Value = new decimal(new int[] {
            49152,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(358, 30);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 19);
            this.label2.TabIndex = 16;
            this.label2.Text = "Номер порта";
            // 
            // textBoxIP
            // 
            this.textBoxIP.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxIP.Location = new System.Drawing.Point(223, 27);
            this.textBoxIP.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxIP.Name = "textBoxIP";
            this.textBoxIP.Size = new System.Drawing.Size(127, 26);
            this.textBoxIP.TabIndex = 19;
            this.textBoxIP.Text = "127.0.0.1";
            this.textBoxIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(208, 19);
            this.label1.TabIndex = 18;
            this.label1.Text = "IP адресс для прослушивания";
            // 
            // groupBoxSettingsServer
            // 
            this.groupBoxSettingsServer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxSettingsServer.Controls.Add(this.buttonStartServer);
            this.groupBoxSettingsServer.Controls.Add(this.label1);
            this.groupBoxSettingsServer.Controls.Add(this.textBoxIP);
            this.groupBoxSettingsServer.Controls.Add(this.label2);
            this.groupBoxSettingsServer.Controls.Add(this.textBoxPort);
            this.groupBoxSettingsServer.Location = new System.Drawing.Point(12, 12);
            this.groupBoxSettingsServer.Name = "groupBoxSettingsServer";
            this.groupBoxSettingsServer.Size = new System.Drawing.Size(710, 73);
            this.groupBoxSettingsServer.TabIndex = 20;
            this.groupBoxSettingsServer.TabStop = false;
            this.groupBoxSettingsServer.Text = "Настройки сервера";
            // 
            // buttonStartServer
            // 
            this.buttonStartServer.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.buttonStartServer.AutoSize = true;
            this.buttonStartServer.Location = new System.Drawing.Point(562, 25);
            this.buttonStartServer.Name = "buttonStartServer";
            this.buttonStartServer.Size = new System.Drawing.Size(136, 29);
            this.buttonStartServer.TabIndex = 22;
            this.buttonStartServer.Text = "Запустить сервер";
            this.buttonStartServer.UseVisualStyleBackColor = true;
            this.buttonStartServer.Click += new System.EventHandler(this.buttonStartServer_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelServer,
            this.statusServerLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 339);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(734, 22);
            this.statusStrip1.TabIndex = 21;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabelServer
            // 
            this.toolStripStatusLabelServer.BackColor = System.Drawing.Color.Red;
            this.toolStripStatusLabelServer.Name = "toolStripStatusLabelServer";
            this.toolStripStatusLabelServer.Size = new System.Drawing.Size(114, 17);
            this.toolStripStatusLabelServer.Text = "Сервер остановлен";
            // 
            // statusServerLabel
            // 
            this.statusServerLabel.Name = "statusServerLabel";
            this.statusServerLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // buttonSelectPdf
            // 
            this.buttonSelectPdf.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSelectPdf.AutoSize = true;
            this.buttonSelectPdf.Location = new System.Drawing.Point(498, 25);
            this.buttonSelectPdf.Name = "buttonSelectPdf";
            this.buttonSelectPdf.Size = new System.Drawing.Size(206, 29);
            this.buttonSelectPdf.TabIndex = 23;
            this.buttonSelectPdf.Text = "Преобразовать PDF в TIFF";
            this.buttonSelectPdf.UseVisualStyleBackColor = true;
            this.buttonSelectPdf.Click += new System.EventHandler(this.buttonSelectPdf_Click);
            // 
            // openFileDialogPdf
            // 
            this.openFileDialogPdf.Filter = "PDF доумент|*.pdf";
            // 
            // saveFileDialogTiff
            // 
            this.saveFileDialogTiff.Filter = "TIFF изображения|*.tiff";
            // 
            // textBoxPdfPath
            // 
            this.textBoxPdfPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxPdfPath.Location = new System.Drawing.Point(190, 27);
            this.textBoxPdfPath.Name = "textBoxPdfPath";
            this.textBoxPdfPath.ReadOnly = true;
            this.textBoxPdfPath.Size = new System.Drawing.Size(302, 26);
            this.textBoxPdfPath.TabIndex = 24;
            // 
            // textBoxTiffPath
            // 
            this.textBoxTiffPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTiffPath.Location = new System.Drawing.Point(6, 62);
            this.textBoxTiffPath.Name = "textBoxTiffPath";
            this.textBoxTiffPath.ReadOnly = true;
            this.textBoxTiffPath.Size = new System.Drawing.Size(486, 26);
            this.textBoxTiffPath.TabIndex = 25;
            // 
            // buttonUploadContent
            // 
            this.buttonUploadContent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonUploadContent.AutoSize = true;
            this.buttonUploadContent.Enabled = false;
            this.buttonUploadContent.Location = new System.Drawing.Point(498, 95);
            this.buttonUploadContent.Name = "buttonUploadContent";
            this.buttonUploadContent.Size = new System.Drawing.Size(206, 29);
            this.buttonUploadContent.TabIndex = 26;
            this.buttonUploadContent.Text = "Загрузить контент в базу";
            this.buttonUploadContent.UseVisualStyleBackColor = true;
            this.buttonUploadContent.Click += new System.EventHandler(this.buttonUploadContent_Click);
            // 
            // buttonUploadTiff
            // 
            this.buttonUploadTiff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonUploadTiff.AutoSize = true;
            this.buttonUploadTiff.Location = new System.Drawing.Point(498, 60);
            this.buttonUploadTiff.Name = "buttonUploadTiff";
            this.buttonUploadTiff.Size = new System.Drawing.Size(206, 29);
            this.buttonUploadTiff.TabIndex = 27;
            this.buttonUploadTiff.Text = "Загрузить файл контента";
            this.buttonUploadTiff.UseVisualStyleBackColor = true;
            this.buttonUploadTiff.Click += new System.EventHandler(this.buttonUploadTiff_Click);
            // 
            // openFileDialogContent
            // 
            this.openFileDialogContent.Filter = "Контент |*.pdf;*.png;*.jpg;*.jpeg;*.tiff;*.tif;*.bmp;*.gif;*.svg";
            // 
            // textBoxNameContent
            // 
            this.textBoxNameContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxNameContent.Location = new System.Drawing.Point(149, 97);
            this.textBoxNameContent.Name = "textBoxNameContent";
            this.textBoxNameContent.Size = new System.Drawing.Size(343, 26);
            this.textBoxNameContent.TabIndex = 28;
            this.textBoxNameContent.TextChanged += new System.EventHandler(this.textBoxNameContent_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(137, 19);
            this.label3.TabIndex = 29;
            this.label3.Text = "Название контента";
            // 
            // groupBoxUploadContent
            // 
            this.groupBoxUploadContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxUploadContent.Controls.Add(this.label5);
            this.groupBoxUploadContent.Controls.Add(this.numericUpDownDpi);
            this.groupBoxUploadContent.Controls.Add(this.label4);
            this.groupBoxUploadContent.Controls.Add(this.textBoxNameContent);
            this.groupBoxUploadContent.Controls.Add(this.buttonSelectPdf);
            this.groupBoxUploadContent.Controls.Add(this.textBoxPdfPath);
            this.groupBoxUploadContent.Controls.Add(this.buttonUploadTiff);
            this.groupBoxUploadContent.Controls.Add(this.label3);
            this.groupBoxUploadContent.Controls.Add(this.textBoxTiffPath);
            this.groupBoxUploadContent.Controls.Add(this.buttonUploadContent);
            this.groupBoxUploadContent.Location = new System.Drawing.Point(12, 91);
            this.groupBoxUploadContent.Name = "groupBoxUploadContent";
            this.groupBoxUploadContent.Size = new System.Drawing.Size(710, 136);
            this.groupBoxUploadContent.TabIndex = 30;
            this.groupBoxUploadContent.TabStop = false;
            this.groupBoxUploadContent.Text = "Загрузка нового контента";
            this.groupBoxUploadContent.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(150, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 19);
            this.label5.TabIndex = 33;
            this.label5.Text = "DPI";
            // 
            // numericUpDownDpi
            // 
            this.numericUpDownDpi.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownDpi.Location = new System.Drawing.Point(84, 27);
            this.numericUpDownDpi.Maximum = new decimal(new int[] {
            175,
            0,
            0,
            0});
            this.numericUpDownDpi.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericUpDownDpi.Name = "numericUpDownDpi";
            this.numericUpDownDpi.Size = new System.Drawing.Size(60, 26);
            this.numericUpDownDpi.TabIndex = 32;
            this.numericUpDownDpi.Value = new decimal(new int[] {
            175,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 19);
            this.label4.TabIndex = 30;
            this.label4.Text = "Качество";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.Location = new System.Drawing.Point(12, 252);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(710, 77);
            this.richTextBox1.TabIndex = 31;
            this.richTextBox1.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 230);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(215, 19);
            this.label6.TabIndex = 32;
            this.label6.Text = "Текущие ключи пользователей";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 361);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.groupBoxUploadContent);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBoxSettingsServer);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(750, 400);
            this.Name = "MainWindow";
            this.Text = "Сервер";
            ((System.ComponentModel.ISupportInitialize)(this.textBoxPort)).EndInit();
            this.groupBoxSettingsServer.ResumeLayout(false);
            this.groupBoxSettingsServer.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBoxUploadContent.ResumeLayout(false);
            this.groupBoxUploadContent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDpi)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown textBoxPort;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxIP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBoxSettingsServer;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelServer;
        private System.Windows.Forms.ToolStripStatusLabel statusServerLabel;
        private System.Windows.Forms.Button buttonStartServer;
        private System.Windows.Forms.Button buttonSelectPdf;
        private System.Windows.Forms.OpenFileDialog openFileDialogPdf;
        private System.Windows.Forms.SaveFileDialog saveFileDialogTiff;
        private System.Windows.Forms.TextBox textBoxPdfPath;
        private System.Windows.Forms.TextBox textBoxTiffPath;
        private System.Windows.Forms.Button buttonUploadContent;
        private System.Windows.Forms.Button buttonUploadTiff;
        private System.Windows.Forms.OpenFileDialog openFileDialogContent;
        private System.Windows.Forms.TextBox textBoxNameContent;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBoxUploadContent;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numericUpDownDpi;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label6;
    }
}

