﻿using Server.Assemblers;
using Server.Assemblers.UsersAssembler;
using System.Collections.Generic;
using Server.Services.Crypto.Kuznechik;
using System;
using Server.Model.ObjectModel;
using Server.Model.Repository.Users;
using Server.Model.Repository;
using DrmCommon.Response;
using DrmCommon.Request;
using DrmCommon.Result;
using Server.Model.Repository.Specification.Users;

namespace Server.Services.ServiceAuthorization
{
    //сервис авторизации, регистрации, проверки прав
    class AuthorizationService
    {
        private readonly ObjectRepository<User> UserRepository = new UserRepository();
        private readonly Assembler<User> UserAssembler;
        private readonly KuznechikCryptoService kuznechikCryptoService;

        public AuthorizationService(KuznechikCryptoService kuznechikCryptoService)
        {
            this.kuznechikCryptoService = kuznechikCryptoService;
            UserAssembler = new UserAssembler(kuznechikCryptoService);
        }

        public ResponseAuthorization MakeOperation (RequestAuthorization requestAuth)
        {
            ResponseAuthorization responseAuthorization = new ResponseAuthorization();

            switch (requestAuth.typeRequest)
            {
                case TypeRequest.Registration:
                    responseAuthorization.resultOperation = this.RegisterNewUser(requestAuth);
                    responseAuthorization.typeResponse = TypeResponse.Registration;
                    break;
                case TypeRequest.RequestAuthorization:
                    responseAuthorization.resultOperation = this.Authorization(requestAuth);
                    responseAuthorization.typeResponse = TypeResponse.ResultAuthorization;
                    break;
                case TypeRequest.CheckAccess:
                    responseAuthorization.resultOperation = this.CheckAccess(requestAuth);
                    responseAuthorization.typeResponse = TypeResponse.CheckAccess;
                    break;
                default:
                    responseAuthorization.resultOperation = new ResultOperation();
                    break;
            }
            return responseAuthorization;
        }

        public ResultOperation RegisterNewUser(RequestAuthorization request)
        {
            ResultOperation resultOperation = new ResultOperation();

            try
            {
                User User = this.UserAssembler.FromDto(request.CurrentUserDtoEncripted, new KeyValuePair<int, string>(request.IDKey , "?"));

                if (UserRepository.create(User))
                {
                    resultOperation.success = true;
                    resultOperation.AddNewInfoMessage("Регистрация успешна");
                }
                else
                {
                    resultOperation.success = false;
                    resultOperation.AddNewErrorMessage("Возникла ошибка при создании нового пользователя");
                }
            }
            catch(Exception exept)
            {
                resultOperation.success = false;
                resultOperation.AddNewErrorMessage(exept.Message);
            }

            return resultOperation;
        }

        public ResultOperation Authorization (RequestAuthorization request)
        {
            ResultOperation resultOperation = new ResultOperation();
            try
            {
                User User = this.UserAssembler.FromDto(
                    request.CurrentUserDtoEncripted, new KeyValuePair<int, string>(request.IDKey, "?"));

                User UsersFromDb = this.UserRepository.selectOne(
                    new UserSpecificationByLoginAndPassword(User.Login, User.PasswordHash));
                kuznechikCryptoService.RemoveTmpKey(request.IDKey, User.Login);
                kuznechikCryptoService.CheckRightKey(request.IDKey, User.Login);
                resultOperation.success = true;

                kuznechikCryptoService.RemoveAllUsersKeysByLogin(User.Login, request.IDKey);
            }
            catch(Exception exept)
            {
                resultOperation.success = false;
                resultOperation.AddNewErrorMessage("Проверьте правильность введенных данных");
                resultOperation.AddNewErrorMessage(exept.Message);
                RemoveKey(request.IDKey);
            }

            return resultOperation;
        }

        public ResultOperation CheckAccess (RequestAuthorization request)
        {
            ResultOperation resultOperation = new ResultOperation();

            try
            {
                User User = this.UserAssembler.FromDto(request.CurrentUserDtoEncripted, 
                    new KeyValuePair<int, string>(request.IDKey, "?"));
                kuznechikCryptoService.CheckRightKey(request.IDKey, User.Login);

                User UserFromDb = this.UserRepository.selectOne(new UserSpecificationByLoginAndPassword(User.Login, User.PasswordHash));

                if (this.CheckAccessForUser(User))
                {
                    resultOperation.success = true;
                }
                else
                {
                    resultOperation.success = false;
                    resultOperation.AddNewErrorMessage("У вас нет доступа");
                }
            }
            catch(Exception exept)
            {
                resultOperation.success = false;
                resultOperation.AddNewErrorMessage(exept.Message);
            }
             
            return resultOperation;
        }

        //проверка прав
        private bool CheckAccessForUser (User User)
        {
            //Проверка доступа
            return true;
        }

        private void RemoveKey (int IDKey)
        {
            try
            {
                kuznechikCryptoService.UsersKeys.Remove(IDKey);
            }
            catch { }
        }
    }
}
