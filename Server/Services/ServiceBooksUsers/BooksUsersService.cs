﻿using Server.Assemblers;
using Server.Assemblers.ContentsAssembler;
using System.Collections.Generic;
using Server.Services.Crypto.Kuznechik;
using System;
using Server.Model.ObjectModel;
using Server.Model.Repository.Users;
using Server.Model.Repository.ContentsUsers;
using DrmCommon.Response;
using DrmCommon.Request;
using DrmCommon.Request.RequestContent;
using DrmCommon.Result;
using Server.Model.Repository.Specification.Users;
using Server.Model.Repository.Specification.Contents;
using Server.Model.Repository.Specification.ContentsUsers;
using DrmCommon.Dto.Contents;
using Server.Model.ObjectModel.UserKey;
using DrmCommon.Helper;
using Server.Model.Repository.Contents;

namespace Server.Services.ServiceContentsUsers
{
    public class ContentsUsersService
    {
        private readonly Assembler<Content> contentAssembler;
        private readonly ContentRepository contentRepository = new ContentRepository();
        private readonly UserRepository userRepository = new UserRepository();
        private readonly ContentsUsersRepository contentsUsersRepository = new ContentsUsersRepository();
        private readonly KuznechikCryptoService kuznechikCryptoService;

        public ContentsUsersService(KuznechikCryptoService kuznechikCryptoService)
        {
            this.kuznechikCryptoService = kuznechikCryptoService;

            contentAssembler = new ContentsAssembler(kuznechikCryptoService);
        }

        public Response MakeOperation (Request request)
        {
            switch (request.typeRequest)
            {
                case TypeRequest.BuyContent:
                    if (request is RequestBuyDeleteContent requestBuyContent)
                    {
                        ResponseBuyDeleteContent responseBuyContent = new ResponseBuyDeleteContent();
                        responseBuyContent.resultOperation = BuyContent(requestBuyContent);
                        responseBuyContent.typeResponse = TypeResponse.BuyContent;
                        return responseBuyContent;                       
                    }
                    break;
                case TypeRequest.DeleteContent:
                    if (request is RequestBuyDeleteContent requestDeleteContent)
                    {
                        ResponseBuyDeleteContent responseDeleteContent = new ResponseBuyDeleteContent();
                        responseDeleteContent.resultOperation = DeleteContent(requestDeleteContent);
                        responseDeleteContent.typeResponse = TypeResponse.DeleteContent;
                        return responseDeleteContent;
                    }
                    break;
                case TypeRequest.GetListContentsAll:
                    if (request is RequestGetListContents requestGetListContents1)
                    {
                        return GetListContents(requestGetListContents1);
                    }
                    break;
                case TypeRequest.GetListUserContents:
                    if (request is RequestGetListContents requestGetListContents2)
                    {
                        return GetListContents(requestGetListContents2);
                    }
                    break;
                case TypeRequest.GetOpenContent:
                    if (request is RequestOpenContent requestOpenContent)
                    {
                        return GetOpenContent(requestOpenContent);
                    }
                    break;
            }
            Response response = new Response();
            response.typeResponse = TypeResponse.None;

            ResultOperation resultOperation = new ResultOperation();
            resultOperation.success = false;
            resultOperation.operation = Operation.None;
            resultOperation.AddNewErrorMessage("Ошибка выполнения операции. Такой операции не существует");
            response.resultOperation = resultOperation;

            return response;
        }

        public ResultOperation BuyContent (RequestBuyDeleteContent requestBuyContent)
        {
            ResultOperation resultOperation = new ResultOperation();
            resultOperation.operation = Operation.BuyContent;

            try
            {
                string LoginUser = kuznechikCryptoService.Decrypt(
                requestBuyContent.userLogin,
                kuznechikCryptoService.GetUserKey(
                    new KeyValuePair<int, string>(requestBuyContent.IDKey, "?"), Crypto.CryptoService.TypeKey.SessionKey));

                kuznechikCryptoService.CheckRightKey(requestBuyContent.IDKey, LoginUser);

                User UserFromDb = userRepository.selectOne(new UserSpecificationByLogin(LoginUser));

                Content content = contentAssembler.FromDto(requestBuyContent.contentDto);
                ContentsUsers contentsUsers = new ContentsUsers(UserFromDb.id, content.id);

                if (contentsUsersRepository.create(contentsUsers))
                {
                    resultOperation.success = true;
                    resultOperation.AddNewInfoMessage("Контент успешно куплен");
                }
                else
                {
                    resultOperation.success = false;
                    resultOperation.AddNewErrorMessage("Произошла ошибка при выполнении операции");
                }
            }
            catch (Exception exept)
            {
                resultOperation.success = false;
                resultOperation.AddNewErrorMessage(exept.Message);
            }
            
            return resultOperation;
        }

        public ResultOperation DeleteContent(RequestBuyDeleteContent requestDeleteContent)
        {
            ResultOperation resultOperation = new ResultOperation();
            resultOperation.operation = Operation.DeleteContent;

            try
            {
                string LoginUser = kuznechikCryptoService.Decrypt(
                requestDeleteContent.userLogin,
                kuznechikCryptoService.GetUserKey(
                    new KeyValuePair<int, string>(requestDeleteContent.IDKey, "?"),
                    Crypto.CryptoService.TypeKey.SessionKey)
                );
                kuznechikCryptoService.CheckRightKey(requestDeleteContent.IDKey, LoginUser);

                User UserFromDb = userRepository.selectOne(new UserSpecificationByLogin(LoginUser));

                Content content = contentAssembler.FromDto(requestDeleteContent.contentDto);
                ContentsUsers contentsUsers = new ContentsUsers(UserFromDb.id, content.id);

                if (contentsUsersRepository.remove(contentsUsers))
                {
                    resultOperation.success = true;
                    resultOperation.AddNewInfoMessage("Контент успешно удален");
                }
                else
                {
                    resultOperation.success = false;
                    resultOperation.AddNewErrorMessage("Произошла ошибка при выполнении операции");
                }
            }
            catch (Exception exept)
            {
                resultOperation.success = false;
                resultOperation.AddNewErrorMessage(exept.Message);
            }

            return resultOperation;
        }

        public ResponseGetListContents GetListContents (RequestGetListContents requestGetListContents)
        {
            ResponseGetListContents responseGetListContents = new ResponseGetListContents();
            ResultOperation resultOperation = new ResultOperation();

            List<Content> contentsModel = new List<Content>();

            if (requestGetListContents.typeRequest == TypeRequest.GetListContentsAll)
            {
                responseGetListContents.typeResponse = TypeResponse.GetListContentsAll;
                contentsModel = contentRepository.select(new ContentSpecificationAll());
                resultOperation.operation = Operation.GetAllContents;
                resultOperation.success = true;
            }
            if (requestGetListContents.typeRequest == TypeRequest.GetListUserContents)
            {
                responseGetListContents.typeResponse = TypeResponse.GetListUserContents;
                resultOperation.operation = Operation.GetContentsForUser;

                try
                {
                    string LoginUser = kuznechikCryptoService.Decrypt(
                        requestGetListContents.userLogin,
                        kuznechikCryptoService.GetUserKey(
                            new KeyValuePair<int, string>(requestGetListContents.IDKey, "?"),
                            Crypto.CryptoService.TypeKey.SessionKey
                            ));
                    kuznechikCryptoService.CheckRightKey(requestGetListContents.IDKey, LoginUser);

                    User UserFromDb = userRepository.selectOne(new UserSpecificationByLogin(LoginUser));

                    List<ContentsUsers> contentsUsers = contentsUsersRepository.select(new ContentsUsersSpecificationByIdUser(UserFromDb.id));
                    if (contentsUsers.Count > 0)
                    {
                        List<int> idsContentsForSelect = new List<int>(contentsUsers.Count);
                        foreach (var contentsUser in contentsUsers)
                        {
                            idsContentsForSelect.Add(contentsUser.idContent);
                        }
                        contentsModel = contentRepository.select(new ContentSpecificationByListId(idsContentsForSelect));
                    }
                    else
                    {
                        contentsModel = new List<Content>();
                    }

                    resultOperation.success = true;
                }
                catch(Exception exept)
                {
                    resultOperation.success = false;
                    resultOperation.AddNewErrorMessage(exept.Message);
                }
            }

            responseGetListContents.listContents = new List<ContentsDto>(contentsModel.Count);
            foreach (var content in contentsModel)
            {
                responseGetListContents.listContents.Add((ContentsDto)contentAssembler.ToDto(content));
            }

            responseGetListContents.resultOperation = resultOperation;

            return responseGetListContents;
        } 

        public ResponseGetOpenContent GetOpenContent (RequestOpenContent requestOpenContent)
        {
            ResponseGetOpenContent responseGetOpenContent = new ResponseGetOpenContent();
            ResultOperation resultOperation = new ResultOperation();

            List<Content> contentsModel = new List<Content>();

            if (requestOpenContent.typeRequest == TypeRequest.GetOpenContent)
            {
                responseGetOpenContent.typeResponse = TypeResponse.GetOpenContent;
                resultOperation.operation = Operation.GetOpenContent;

                try
                {
                    string LoginUser = kuznechikCryptoService.Decrypt(
                        requestOpenContent.userLogin,
                        kuznechikCryptoService.GetUserKey(
                            new KeyValuePair<int, string>(requestOpenContent.IDKey, "?"),
                            Crypto.CryptoService.TypeKey.SessionKey
                        ));
                    kuznechikCryptoService.CheckRightKey(requestOpenContent.IDKey, LoginUser);

                    User UserFromDb = userRepository.selectOne(new UserSpecificationByLogin(LoginUser));

                    ContentsUsers contentsUser = contentsUsersRepository.selectOne(
                        new ContentsUsersSpecificationByIdUserAndIdContent(UserFromDb.id, requestOpenContent.contentDto.id)
                        );

                    Content content = contentRepository.selectOne(new ContentSpecificationById(requestOpenContent.contentDto.id, true));

                    //Проверка ключа для контента
                    StructureUserKey structureUserKey = kuznechikCryptoService.UsersKeys[requestOpenContent.IDKey];
                    if (structureUserKey.KeyForContent == new BigInteger())
                    {
                        structureUserKey.GenerateKeyForContent();
                    }
                    responseGetOpenContent.encriptedKeyForContent = kuznechikCryptoService.Encrypt(
                        structureUserKey.KeyForContent.ToHexString(),
                        kuznechikCryptoService.GetUserKey(
                            new KeyValuePair<int, string>(requestOpenContent.IDKey, LoginUser),
                            Crypto.CryptoService.TypeKey.SessionKey
                        ));

                    ContentsDto contentsDto = (ContentsDto)contentAssembler.ToDto(content, 
                        new KeyValuePair<int, string>(requestOpenContent.IDKey, LoginUser));

                    resultOperation.success = true;
                    responseGetOpenContent.resultOperation = resultOperation;
                    responseGetOpenContent.content = contentsDto;
                }
                catch(Exception exept)
                {
                    resultOperation.success = false;
                    resultOperation.AddNewErrorMessage(exept.Message);
                    responseGetOpenContent.resultOperation = resultOperation;
                }

                return responseGetOpenContent;
            }

            resultOperation.success = false;
            resultOperation.operation = Operation.GetOpenContent;
            resultOperation.AddNewErrorMessage("Произошла ошибка при загрузке контента из базы");

            responseGetOpenContent.typeResponse = TypeResponse.GetOpenContent;
            responseGetOpenContent.resultOperation = resultOperation;

            return responseGetOpenContent;
        }
    }
}