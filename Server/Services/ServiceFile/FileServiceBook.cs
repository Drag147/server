﻿using Server.Services.ServiceFile.FileConverter;
using System.IO;
using DrmCommon.Result;
using Server.Model.ObjectModel;
using Server.Model.Repository.Contents;

namespace Server.Services.ServiceFile
{
    public class FileServiceContent
    {
        //public MagickImageCollection ImageTiff { get; private set; }
        public byte[] DataContent { get; private set; }
        private readonly PdfToTiffConverter FileConverter = new PdfToTiffConverter();
        private readonly ContentRepository repository = new ContentRepository();

        public FileServiceContent()
        {

        }

        public void ClearImage()
        {
            if (DataContent != null)
            {
                DataContent = null;
            }
        }

        //С сохранение в файл
        public ResultOperation ConvertPdf2Tiff(string FilePathOpen = null, string FilePathSave = null, int Dpi = 150)
        {
            if (FilePathOpen == null || FilePathSave == null)
            {
                return new ResultOperation()
                {
                    operation = Operation.ConvertContentOnServer,
                    success = false,
                    ErrorMessages = {
                        "Файл не выбран или выбран некорректно",
                        "Некорректно выбран путь для сохранения"
                    }
                };
            }

            if (FileConverter.PdfToTiff(FilePathOpen, FilePathSave, Dpi))
            {
                UploadTiff(FilePathSave);
                return new ResultOperation()
                {
                    operation = Operation.ConvertContentOnServer,
                    success = true,
                    InfoMessages = {
                        "Файл успешно конвертирован и готов к загрузке в базу"
                    }
                };
            }
            else
            {
                return new ResultOperation()
                {
                    operation = Operation.ConvertContentOnServer,
                    success = false,
                    ErrorMessages = {
                        "Произошла ошибка при конвертации файла"
                    }
                };
            }
        }

        //загрузить из файла
        public ResultOperation UploadTiff(string FilePathOpen = null)
        {
            if (FilePathOpen == null)
            {
                return new ResultOperation()
                {
                    operation = Operation.UploadContent,
                    success = false,
                    ErrorMessages = { "Файл не выбран или выбран некорректно" }
                };
            }

            DataContent = File.ReadAllBytes(FilePathOpen);
        
            if (DataContent != null)
            {
                return new ResultOperation()
                {
                    operation = Operation.UploadContent,
                    success = true,
                    InfoMessages = { "Файл успешно загружен" }
                };
            }
            else
            {
                return new ResultOperation()
                {
                    operation = Operation.UploadContent,
                    success = false,
                    ErrorMessages = { "Произошла ошибка при загрзке файла" }
                };
            }
        }

        //загрузить в базу
        public ResultOperation UploadSelectedTiffInDb(string NameContent)
        {
            Content content = new Content()
            {
                ContentData = DataContent,
                NameContent = NameContent
            };

            if (repository.create(content))
            {
                return new ResultOperation()
                {
                    operation = Operation.UploadContent,
                    success = true,
                    InfoMessages =
                    {
                        "Контент успешно загружен"
                    }
                };
            }
            else
            {
                return new ResultOperation()
                {
                    operation = Operation.UploadContent,
                    success = false,
                    ErrorMessages =
                    {
                        "Ошибка при загрузке контента"
                    }
                };
            }
        }
    }
}
