﻿using ImageMagick;
using NLog;
using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace Server.Services.ServiceFile.FileConverter
{
    class PdfToTiffConverter
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        public bool PdfToTiff(string PathToPdf, string PathToTiff, int Dpi)
        {
            try
            {
                MagickReadSettings settings = new MagickReadSettings
                {
                    Density = new Density(Dpi, Dpi),
                    Compression = CompressionMethod.JPEG
                };
                using (MagickImageCollection images = new MagickImageCollection(PathToPdf, settings))
                {
                    CreateFileTiff(images, PathToTiff);
                }
                return true;
            }
            catch (Exception exept)
            {
                logger.Error(exept);
                return false;
            }
        }

        private void CreateFileTiff(MagickImageCollection images, string path)
        {
            ImageCodecInfo inf = GetEncoderInfo("image/tiff");
            Encoder saveFlag = Encoder.SaveFlag;
            EncoderParameters myEncoderParameters = new EncoderParameters(1);

            Bitmap img = images[0].ToBitmap();

            try
            {
                myEncoderParameters.Param[0] = new EncoderParameter(saveFlag, (long)EncoderValue.MultiFrame);
                img.Save(path, inf, myEncoderParameters);

                for (int i = 1; i < images.Count; i++)
                {
                    Bitmap img2 = images[i].ToBitmap();
                    myEncoderParameters.Param[0] = new EncoderParameter(saveFlag, (long)EncoderValue.FrameDimensionPage);
                    img.SaveAdd(img2, myEncoderParameters);
                }
            }
            catch (Exception exept)
            {
                throw exept;
            }
            finally
            {
                myEncoderParameters.Param[0] = new EncoderParameter(saveFlag, (long)EncoderValue.Flush);
                img.SaveAdd(myEncoderParameters);
            }
        }

        private ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            // Get image codecs for all image formats
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

            // Find the correct image codec
            for (int i = 0; i < codecs.Length; i++)
                if (codecs[i].MimeType == mimeType)
                    return codecs[i];
            return null;
        }
    }
}