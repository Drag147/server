﻿using DrmCommon.Helper;
using DrmCommon.Request;
using DrmCommon.Response;
using DrmCommon.Result;
using NLog;
using Server.Model.ObjectModel.UserKey;
using Server.Services.Crypto.GenerateSessionKey;
using System;
using System.Collections.Generic;

namespace Server.Services.Crypto
{
    public abstract class CryptoService
    {
        public enum TypeKey
        {
            SessionKey,
            ContentKey
        }

        private Random rand;
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        public readonly Dictionary<int, StructureUserKey> UsersKeys = new Dictionary<int, StructureUserKey>();
        

        private readonly GenerateSessionKeyService generateSessionKeyService;

        public CryptoService(Random rand)
        {
            this.rand = rand;
            generateSessionKeyService = new GenerateSessionKeyService(rand, ref UsersKeys);
        }

        public abstract string Encrypt(string data, byte[] key);
        public abstract string Decrypt(string data, byte[] key);
        public abstract byte[] EncryptContent(byte[] data, byte[] key);

        public ResultOperation AddNewSessionUserKey(int IDKey, BigInteger SessionKey)
        {
            ResultOperation resultOperation = new ResultOperation();
            resultOperation.operation = Operation.AddNewSessionKey;
            try
            {
                StructureUserKey structureUserKeys = new StructureUserKey(rand);
                structureUserKeys.SessionKey = SessionKey;
                UsersKeys.Add(IDKey, structureUserKeys);
                resultOperation.success = true;
            }
            catch (Exception exept)
            {
                resultOperation.success = false;
                resultOperation.AddNewErrorMessage(exept.Message);
            }
            return resultOperation;
        }

        public ResultOperation GenerateContentKeys(int IDKey)
        {
            ResultOperation resultOperation = new ResultOperation();
            resultOperation.operation = Operation.GenerateNewContent;
            try
            {
                if (UsersKeys.TryGetValue(IDKey, out StructureUserKey structureUserKeys))
                {
                    structureUserKeys.GenerateKeyForContent();
                    resultOperation.success = true;
                }
                resultOperation.success = false;
                resultOperation.AddNewErrorMessage("Не удалось создать ключ. Такой пользователь не имеет сессии.");
            }
            catch (Exception exept)
            {
                resultOperation.success = false;
                resultOperation.AddNewErrorMessage(exept.Message);
            }
            return resultOperation;
        }

        public ResponseGenerateSessionKeys MakeOperation(RequestGenerateSessionKeys requestGenerateSessionKeys)
        {
            if (requestGenerateSessionKeys.IDKey != 0 && requestGenerateSessionKeys.userLogin != null
                && requestGenerateSessionKeys.typeRequest == TypeRequest.GenerateSessionKeyStep1)
            {
                try
                {
                    string loginDecr = Decrypt(requestGenerateSessionKeys.userLogin,
                        UsersKeys[requestGenerateSessionKeys.IDKey].SessionKey.getBytes());
                    CheckRightKey(requestGenerateSessionKeys.IDKey, loginDecr);
                    UsersKeys.Remove(requestGenerateSessionKeys.IDKey);
                    try { generateSessionKeyService.RemoveTmpKey(requestGenerateSessionKeys.IDKey); } catch { }
                    generateSessionKeyService.RegenerateUsersLogin.Add(new KeyValuePair<int, string>(-1, loginDecr));
                    requestGenerateSessionKeys.userLogin = loginDecr;
                }
                catch
                {
                    throw new Exception("Произошла ошибка при обновлении ваших ключей. Авторизируйтесь заново.");
                }

            }
            return generateSessionKeyService.MakeOperation(requestGenerateSessionKeys);
        }

        public void RemoveAllUsersKeysByLogin(string login, int excludeIdKey)
        {
            try
            {
                HashSet<int> IdsKeysForDelete = new HashSet<int>();

                foreach (var item in this.UsersKeys)
                {
                    if (item.Value.login == login && item.Key != excludeIdKey)
                    {
                        IdsKeysForDelete.Add(item.Key);
                    }
                }

                foreach (var item in IdsKeysForDelete)
                {
                    this.UsersKeys.Remove(item);
                }
            }
            catch { }
        }

        public void RemoveUsersKey(int IDKey, string login)
        {
            try
            {
                string loginDecr = Decrypt(login, UsersKeys[IDKey].SessionKey.getBytes());
                CheckRightKey(IDKey, loginDecr);
                UsersKeys.Remove(IDKey);
                generateSessionKeyService.RemoveTmpKey(IDKey);
            }
            catch { }
        }

        public void CheckRightKey(int IDKey, string login)
        {
            try
            {
                if (UsersKeys.TryGetValue(IDKey, out StructureUserKey userKey))
                {
                    if (userKey.login == login)
                    {
                        return;
                    }
                    else
                    {
                        UsersKeys.Remove(IDKey);
                    }
                }
            }
            catch { }

            throw new ArgumentNullException("Такой ключи у пользователя не обнаружен");
        }

        public bool CheckNeedUpdateKey(int IDKey, string EncriptedLogin)
        {
            try
            {
                string login = this.Decrypt(EncriptedLogin, this.UsersKeys[IDKey].SessionKey.getBytes());
                CheckRightKey(IDKey, login);
                if (UsersKeys.TryGetValue(IDKey, out StructureUserKey userKey))
                {
                    if (userKey.login == login && userKey.UpdateKeys)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch { UsersKeys.Remove(IDKey); }

            return false;
        }

        public byte[] GetUserKey(KeyValuePair<int, string> IDKeyAndLogin, TypeKey typeKey)
        {
            try
            {
                if (UsersKeys.TryGetValue(IDKeyAndLogin.Key, out StructureUserKey userKey))
                {
                    if (userKey.login == IDKeyAndLogin.Value || IDKeyAndLogin.Value == "?")
                    {
                        switch (typeKey)
                        {
                            case TypeKey.SessionKey:
                                return userKey.SessionKey.getBytes();
                            case TypeKey.ContentKey:
                                return userKey.KeyForContent.getBytes();
                        }
                    }
                    else
                    {
                        UsersKeys.Remove(IDKeyAndLogin.Key);
                    }
                }
            }
            catch { }
            throw new ArgumentNullException("Не удалось найти пользователя с таким ID ключа и логином");
        }

        public void RemoveTmpKey(int IDKey, string login)
        {
            generateSessionKeyService.RemoveTmpKey(IDKey);

            if (!AddLoginInSessionRecord(IDKey, login))
            {
                throw new ArgumentNullException("Ошибка. Не удалось корректно обновить сессионную запись пользователять.");
            }
        }

        private bool AddLoginInSessionRecord(int IDKey, string login)
        {
            try
            {
                if (UsersKeys.TryGetValue(IDKey, out StructureUserKey userKey))
                {
                    if (userKey.login == "?")
                    {
                        userKey.login = login;
                        return true;
                    }
                }
            }
            catch
            {

            }
            return false;
        }
    }
}