﻿using NLog;
using Server.Assemblers.SessionKeyGenerateAssembler;
using System;
using System.Collections.Generic;
using DrmCommon.Response;
using DrmCommon.Request;
using Server.Model.ObjectModel.UserKey;
using Server.Model.ObjectModel.ObjectModel;
using DrmCommon.Result;
using DrmCommon.Dto.SessionKey;
using DrmCommon.Helper;

namespace Server.Services.Crypto.GenerateSessionKey
{
    public class GenerateSessionKeyService
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private Random rand;
        private SessionKeyGenerateAssembler SessionKeyGenerateAssembler = new SessionKeyGenerateAssembler();
        private readonly Dictionary<int, StructureUserKey> UsersKeys;
        //мапа с временными ключами при генерации после 1 шага, потом они удаляются после авторизации
        private readonly Dictionary<int, SessionKeyGenerate> TmpSessionKey = new Dictionary<int, SessionKeyGenerate>();
        public readonly HashSet<KeyValuePair<int, string>> RegenerateUsersLogin = new HashSet<KeyValuePair<int, string>>();

        public GenerateSessionKeyService(Random rand, ref Dictionary<int, StructureUserKey> UsersKeys)
        {
            this.rand = rand;
            this.UsersKeys = UsersKeys;
        }

        public ResponseGenerateSessionKeys MakeOperation(RequestGenerateSessionKeys requestGenerateSessionKeys)
        {
            ResponseGenerateSessionKeys responseGenerateSessionKeys = new ResponseGenerateSessionKeys();
            ResultOperation resultOperation = new ResultOperation();

            switch (requestGenerateSessionKeys.typeRequest)
            {
                case TypeRequest.GenerateSessionKeyStep1:
                    try
                    {
                        SessionKeyGenerate sessionKeyGenerate = GenerateSessionKeyStep1(requestGenerateSessionKeys.userLogin);
                        responseGenerateSessionKeys.sessionKeyGenerateDto =
                            (SessionKeyGenerateDto)SessionKeyGenerateAssembler.ToDto(sessionKeyGenerate);
                        resultOperation.success = true;
                    }
                    catch (Exception exept)
                    {
                        resultOperation.success = false;
                        resultOperation.AddNewErrorMessage(exept.Message);
                    }
                    responseGenerateSessionKeys.resultOperation = resultOperation;
                    responseGenerateSessionKeys.typeResponse = TypeResponse.GenerateSessionKeyStep1;
                    break;
                case TypeRequest.GenerateSessionKeyStep2:
                    try
                    {
                        resultOperation = GenerateSessionKeyStep2(
                            SessionKeyGenerateAssembler.FromDto(requestGenerateSessionKeys.sessionKeyGenerateDto),
                            requestGenerateSessionKeys.userLogin);
                        responseGenerateSessionKeys.sessionKeyGenerateDto = null;
                    }
                    catch (Exception exept)
                    {
                        resultOperation.success = false;
                        resultOperation.AddNewErrorMessage(exept.Message);
                    }
                    responseGenerateSessionKeys.resultOperation = resultOperation;
                    responseGenerateSessionKeys.typeResponse = TypeResponse.GenerateSessionKeyStep2;
                    break;
                default:
                    responseGenerateSessionKeys.resultOperation = new ResultOperation();
                    responseGenerateSessionKeys.resultOperation.success = false;
                    responseGenerateSessionKeys.resultOperation.AddNewErrorMessage("Ошибка. Такая операция не обнаружена.");
                    break;
            }
            return responseGenerateSessionKeys;
        }

        public void RemoveTmpKey(int IDKey)
        {
            try
            {
                TmpSessionKey.Remove(IDKey);
            }
            catch
            {
            }
        }

        //Генерация N, G, X
        private SessionKeyGenerate GenerateSessionKeyStep1(string loginRegenerate = null)
        {
            SessionKeyGenerate sessionKeyStep1 = new SessionKeyGenerate();

            int maxCount = 50;
            int nowCount = 0;

            while (nowCount < maxCount)
            {
                sessionKeyStep1.N = BigInteger.genPseudoPrime(256, 1000, rand);
                sessionKeyStep1.G = PrimitivRoot(sessionKeyStep1.N);
                if (sessionKeyStep1.G == -1)
                {
                    continue;
                }
                sessionKeyStep1.Xs = new BigInteger();
                sessionKeyStep1.Xs.genRandomBits(256, rand);
                sessionKeyStep1.X = sessionKeyStep1.G.modPow(sessionKeyStep1.Xs, sessionKeyStep1.N);

                if (CheckSessionKey(sessionKeyStep1))
                {
                    int IDKey = rand.Next();
                    while (TmpSessionKey.ContainsKey(IDKey))
                    {
                        IDKey = rand.Next();
                    }

                    logger.Debug("Сгенерированы N, G, X для первого шага. IDKey: " + IDKey);

                    sessionKeyStep1.IDKey = IDKey;

                    if (loginRegenerate != null)
                    {
                        if (RegenerateUsersLogin.Contains(new KeyValuePair<int, string>(-1, loginRegenerate)))
                        {
                            RegenerateUsersLogin.Remove(new KeyValuePair<int, string>(-1, loginRegenerate));
                            RegenerateUsersLogin.Add(new KeyValuePair<int, string>(IDKey, loginRegenerate));
                        }
                        else
                        {
                            throw new Exception("Произошла ошибка при обновлении ключей");
                        }
                    }

                    TmpSessionKey.Add(IDKey, sessionKeyStep1);
                    return sessionKeyStep1;
                }
                nowCount++;
            }

            logger.Error("Ошибка при генерации сеансового ключа на шаге 1");
            throw new ArgumentException("Ошибка при генерации сеансового ключа на первом шаге");
        }

        //По принятой Y генерируем Key
        private ResultOperation GenerateSessionKeyStep2(SessionKeyGenerate sessionKeyGenerate, string EncriptedLoginOnNewKey = null)
        {
            ResultOperation resultOperation = new ResultOperation();
            try
            {
                if(TmpSessionKey.TryGetValue(sessionKeyGenerate.IDKey, out SessionKeyGenerate SessionKeyInMap))
                {
                    SessionKeyInMap.Key = sessionKeyGenerate.Y.modPow(SessionKeyInMap.Xs, SessionKeyInMap.N);

                    StructureUserKey structureUserKey = new StructureUserKey(rand);
                    structureUserKey.login = "?";
                    structureUserKey.SetSessionKey(SessionKeyInMap.Key);

                    //либо сразу кладём в UserKeys с логином (при обновлении ключей)
                    if (EncriptedLoginOnNewKey != null)
                    {
                        string DecriptLogin = Kuznechik.KuznechikCryptoService.getinstantiate(rand)
                            .Decrypt(EncriptedLoginOnNewKey,SessionKeyInMap.Key.getBytes());

                        if (RegenerateUsersLogin.Contains(
                            new KeyValuePair<int, string>(sessionKeyGenerate.IDKey, DecriptLogin)))
                        {
                            structureUserKey.login = DecriptLogin;
                            TmpSessionKey.Remove(sessionKeyGenerate.IDKey);
                            RegenerateUsersLogin.Remove(
                                new KeyValuePair<int, string>(sessionKeyGenerate.IDKey, DecriptLogin));
                        }
                    }

                    UsersKeys.Add(sessionKeyGenerate.IDKey, structureUserKey);

                    resultOperation.success = true;
                }
            }
            catch(Exception exept)
            {
                resultOperation.success = false;
                resultOperation.AddNewErrorMessage(exept.Message);
            }

            return resultOperation;
        }

        private BigInteger PrimitivRoot(BigInteger N)
        {
            BigInteger G = new BigInteger();

            int maxCount = 100;
            int nowCount = 0;
            while (nowCount < maxCount)
            {
                G.genRandomBits(256, rand);

                if (G < N)
                {
                    return G;
                }

                nowCount++;
            }

            return -1;

            /*List<BigInteger> fact = new List<BigInteger>();
            BigInteger phi = N - 1, n = phi;
            for (BigInteger i = 2; i * i <= n; ++i)
            {
                if (n % i == 0)
                {
                    fact.Add(i);
                    while (n % i == 0)
                    {
                        n /= i;
                    }
                }
            }
            if (n > 1)
            {
                fact.Add(n);
            }

            for (BigInteger res = 2; res <= N; ++res)
            {
                bool ok = true;
                for (int i = 0; i < fact.Count && ok; ++i)
                {
                    ok &= res.modPow(phi / fact[i], N) != 1;
                }
                if (ok)
                {
                    return res;
                }
            }
            return -1;*/
        }

        private bool CheckSessionKey(SessionKeyGenerate sessionKeyForCheck)
        {
            if (sessionKeyForCheck.N <= sessionKeyForCheck.Xs)
            {
                return false;
            }

            if (sessionKeyForCheck.N <= sessionKeyForCheck.G)
            {
                return false;
            }

            if (sessionKeyForCheck.N <= sessionKeyForCheck.X)
            {
                return false;
            }

            return true;
        }
    }
}