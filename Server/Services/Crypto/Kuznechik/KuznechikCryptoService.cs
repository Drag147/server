﻿using System;
using System.Numerics;
using System.Text;

namespace Server.Services.Crypto.Kuznechik
{
    public class KuznechikCryptoService : CryptoService
    {
        private const string SYMBOL_FOR_ADD_END_BLOCK = "\0";

        private static KuznechikCryptoService instance = null;
        private readonly KuznechikOnVectors kuznechikOnVectors;

        private KuznechikCryptoService(Random rand) : base(rand)
        {
            kuznechikOnVectors = new KuznechikOnVectors();
        }

        public override string Encrypt(string data, byte[] key)
        {
            Vector<byte>[] roundKeys = new Vector<byte>[10];
            kuznechikOnVectors.generateEncryptionRoundKeys(key, ref roundKeys);

            byte[] textBytes = Encoding.GetEncoding(1251).GetBytes(data);

            if (textBytes.Length % 16 != 0)
            {
                textBytes.CopyTo(textBytes = new byte[textBytes.Length + 16 - (textBytes.Length % 16)], 0);
            }

            byte[] block = new byte[16];
            Vector<byte> tmp;

            for (int i = 0; i < textBytes.Length / 16; i++)
            {
                tmp = new Vector<byte>(textBytes, i * 16);
                kuznechikOnVectors.encrypt(ref tmp, roundKeys);
                tmp.CopyTo(block, 0);
                Buffer.BlockCopy(block, 0, textBytes, i * 16, 16);
            }

            return Encoding.GetEncoding(1251).GetString(textBytes);
        }

        public override string Decrypt(string data, byte[] key)
        {
            Vector<byte>[] roundKeys = new Vector<byte>[10];
            kuznechikOnVectors.generateDencryptionRoundKeys(key, ref roundKeys);

            byte[] textBytes = Encoding.GetEncoding(1251).GetBytes(data);

            if (textBytes.Length % 16 != 0)
            {
                textBytes.CopyTo(textBytes = new byte[textBytes.Length + 16 - (textBytes.Length % 16)], 0);
            }

            byte[] block = new byte[16];
            Vector<byte> tmp;

            for (int i = 0; i < textBytes.Length / 16; i++)
            {
                tmp = new Vector<byte>(textBytes, i * 16);
                kuznechikOnVectors.decrypt(ref tmp, roundKeys);
                tmp.CopyTo(block, 0);
                Buffer.BlockCopy(block, 0, textBytes, i * 16, 16);
            }

            return Encoding.GetEncoding(1251).GetString(textBytes).Replace(SYMBOL_FOR_ADD_END_BLOCK, "");
        }

        public override byte[] EncryptContent(byte[] textBytes, byte[] key)
        {
            Vector<byte>[] roundKeys = new Vector<byte>[10];

            kuznechikOnVectors.generateEncryptionRoundKeys(key, ref roundKeys);

            if (textBytes.Length % 16 != 0)
            {
                textBytes.CopyTo(textBytes = new byte[textBytes.Length + 16 - (textBytes.Length % 16)], 0);
            }

            byte[] block = new byte[16];
            Vector<byte> tmp;

            for (int i = 0; i < textBytes.Length / 16; i++)
            {
                tmp = new Vector<byte>(textBytes, i * 16);
                kuznechikOnVectors.encrypt(ref tmp, roundKeys);
                tmp.CopyTo(block, 0);
                Buffer.BlockCopy(block, 0, textBytes, i * 16, 16);
            }

            return textBytes;
        }

        public static KuznechikCryptoService getinstantiate(Random rand)
        {
            if (instance == null)
            {
                instance = new KuznechikCryptoService(rand);
            }
            return instance;
        }

        private string[] StringSplitter128(string data)
        {
            //8 бит на символ Windows-1251 128-bit блок
            int chunkSize = 16;
            int inputStringSize = data.Length;
            double doubleVarChunkCount = (double)inputStringSize / (double)chunkSize;
            int chunkCount = (int)Math.Ceiling(doubleVarChunkCount);
            if (inputStringSize < chunkCount * chunkSize)
            {
                for (int j = inputStringSize; j < chunkCount * chunkSize; j++)
                {
                    data += SYMBOL_FOR_ADD_END_BLOCK;
                }
            }

            string[] block = new string[chunkCount];
            for (int i = 0; i < chunkCount; i++)
            {
                block[i] = data.Substring(i * chunkSize, chunkSize);
            }

            return block;
        }
    }
}