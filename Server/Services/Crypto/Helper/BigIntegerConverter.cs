﻿using DrmCommon.Helper;
using Newtonsoft.Json;
using System;

namespace Server.Services.Crypto.Helper
{
    public class BigIntegerConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(BigInteger);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value == null)
            {
                return null;
            }
            BigInteger bigInteger = new BigInteger((string)reader.Value, 16);
            return bigInteger;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            BigInteger bigInteger = (BigInteger)value;
            serializer.Serialize(writer, bigInteger.ToHexString(), typeof(BigInteger));
        }
    }
}
