﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Quartz;
using Server.Model.ObjectModel.UserKey;

namespace Server.Jobs
{
    public class JobCheckDeleteUsersKeys : IJob
    {
        public Task Execute(IJobExecutionContext context)
        {
            if (context.Trigger.Key.Name.Equals("CheckDeleteKeysUsers"))
            {
                var UsersKeys = context.JobDetail.JobDataMap.Get("UsersKeys") as Dictionary<int, StructureUserKey>;
                int IntervalCheck = (int)context.JobDetail.JobDataMap.Get("IntervalCheck");
                HashSet<int> RemoveKeysId = new HashSet<int>();
                foreach (var item in UsersKeys)
                {
                    if (item.Value.dateTimeSpanForKeyContent.AddMinutes(IntervalCheck) <= DateTime.Now 
                        && item.Value.UpdateKeys)
                    {
                        RemoveKeysId.Add(item.Key);
                    }
                }

                foreach (var item in RemoveKeysId)
                {
                    try
                    {
                        UsersKeys.Remove(item);
                    }
                    catch { }
                }
            }
            return null;
        }
    }
}
