﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Quartz;
using Server.Model.ObjectModel.UserKey;

namespace Server.Jobs
{
    public class JobCheckUpdateUsersKeys : IJob
    {
        public Task Execute(IJobExecutionContext context)
        {
            if (context.Trigger.Key.Name.Equals("CheckUpdateKeysUsers"))
            {
                var UsersKeys = context.JobDetail.JobDataMap.Get("UsersKeys") as Dictionary<int, StructureUserKey>;
                int IntervalCheck = (int)context.JobDetail.JobDataMap.Get("IntervalCheck");
                foreach (var item in UsersKeys)
                {
                    if (item.Value.dateTimeSpanForKeyContent.AddMinutes(IntervalCheck) <= DateTime.Now 
                        && !item.Value.UpdateKeys)
                    {
                        item.Value.UpdateKeys = true;
                        item.Value.dateTimeSpanForKeyContent = DateTime.Now;
                    }
                }
            }
            return null;
        }
    }
}
