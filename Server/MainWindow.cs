﻿using System;
using System.Drawing;
using System.Net;
using System.Windows.Forms;
using Server.Services.ServiceAuthorization;
using Server.Services.ServiceFile;
using Server.Services.ServiceContentsUsers;
using Server.Services.Crypto.Kuznechik;
using System.Threading.Tasks;
using DrmCommon.Request;
using DrmCommon.Response;
using DrmCommon.Request.RequestContent;
using DrmCommon.Result;

namespace Server
{
    public partial class MainWindow : Form
    {
        private Random rand = new Random();

        private ServerScript.Server server = new ServerScript.Server();
        private AuthorizationService authorizationService;
        private ContentsUsersService contentsUsersService;
        private FileServiceContent fileServiceContent = new FileServiceContent();
        private KuznechikCryptoService kuznechikCryptoService;

        public MainWindow()
        {
            InitializeComponent();
            this.server.AcceptedData += this.AceptedData;

            kuznechikCryptoService = KuznechikCryptoService.getinstantiate(rand);

            authorizationService = new AuthorizationService(kuznechikCryptoService);
            contentsUsersService = new ContentsUsersService(kuznechikCryptoService);
        }

        private void AceptedData(Request request)
        {
            //принятие данных
            //Выход
            if (request is RequestLogout requestLogout)
            {
                kuznechikCryptoService.RemoveUsersKey(requestLogout.IDKey, requestLogout.login);
                return;
            }
            //Генерация сеансовых ключей
            if (request is RequestGenerateSessionKeys requestGenerateSessionKeys)
            {
                ResponseGenerateSessionKeys responseGenerate = kuznechikCryptoService.MakeOperation(requestGenerateSessionKeys);
                server.SendData(requestGenerateSessionKeys.socket, responseGenerate);
                return;
            }
            //Запрос авторизации
            if (request is RequestAuthorization requestAuth)
            {
                ResponseAuthorization responseAuthorization = authorizationService.MakeOperation(requestAuth);
                server.SendData(requestAuth.socket, responseAuthorization);
                return;
            }
            //Запрос контента
            if (request is RequestContent requestContent)
            {
                Response response = contentsUsersService.MakeOperation(request);
                if (request.typeRequest != TypeRequest.GetListContentsAll &&
                    kuznechikCryptoService.CheckNeedUpdateKey(request.IDKey, requestContent.userLogin))
                {
                    response.NeedUpdateKey = true;
                }
                server.SendData(request.socket, response);
                return;
            }
        }

        private void buttonStartServer_Click(object sender, System.EventArgs e)
        {
            if (!IPAddress.TryParse(textBoxIP.Text, out IPAddress ipServer))
            {
                MessageBox.Show("Неправильный формат IP адреса", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                return;
            }

            if (this.server.Start(
                ipServer,
                Convert.ToInt32(textBoxPort.Text),
                Properties.Settings.Default.numberAsyncOperation,
                Properties.Settings.Default.maxBytesForResive))
            {
                textBoxIP.Enabled = false;
                textBoxPort.Enabled = false;

                buttonStartServer.Enabled = false;

                toolStripStatusLabelServer.Text = "Сервер запущен";
                toolStripStatusLabelServer.BackColor = Color.Green;

                this.groupBoxUploadContent.Show();

                UpdateUsersKeys();

                Schedulers.CheckDeleteUsersKeysSchedulers.Start(kuznechikCryptoService.UsersKeys,
                    Properties.Settings.Default.timeCheckDeleteKeysUser,
                    Properties.Settings.Default.timeToDeleteKeys);
                Schedulers.CheckUpdateUsersKeysSchedulers.Start(kuznechikCryptoService.UsersKeys,
                    Properties.Settings.Default.timeCheckUpdateKeysUser,
                    Properties.Settings.Default.timeToUpdateKeys);
            }
            else
            {
                MessageBox.Show("Не удалось запустить сервер", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                return;
            }
        }

        private void buttonSelectPdf_Click(object sender, EventArgs e)
        {
            if (this.openFileDialogPdf.ShowDialog() == DialogResult.OK)
            {
                this.textBoxPdfPath.Text = this.openFileDialogPdf.FileName;

                ResultOperation resultConvert = null;

                string fileSaveName = this.openFileDialogPdf.SafeFileName;
                fileSaveName = fileSaveName.Replace("pdf", "tiff");

                this.saveFileDialogTiff.FileName = fileSaveName;
                if (this.saveFileDialogTiff.ShowDialog() == DialogResult.OK)
                {
                    this.textBoxTiffPath.Text = this.saveFileDialogTiff.FileName;

                    resultConvert = this.fileServiceContent.ConvertPdf2Tiff(this.openFileDialogPdf.FileName, this.saveFileDialogTiff.FileName,
                        Convert.ToInt32(numericUpDownDpi.Value));

                    if (resultConvert.success)
                    {
                        this.textBoxPdfPath.Clear();

                        this.openFileDialogPdf.FileName = "";
                        this.numericUpDownDpi.Value = 175;

                        this.textBoxNameContent.Text = fileSaveName.Replace(".tiff", "");
                    }
                    else
                    {
                        this.ResetFormFields();
                    }
                }
                else
                {
                    resultConvert = new ResultOperation()
                    {
                        success = false
                    };
                    resultConvert.AddNewErrorMessage("Нужно выбрать место и имя файла для сохранения");

                    this.ResetFormFields();
                }

                this.ShowResultMessagesAndErrors(resultConvert);
            }
        }

        private void ResetFormFields()
        {
            fileServiceContent.ClearImage();

            this.textBoxPdfPath.Clear();
            this.textBoxTiffPath.Clear();
            this.fileServiceContent.ClearImage();

            this.saveFileDialogTiff.FileName = "";
            this.openFileDialogPdf.FileName = "";
            this.numericUpDownDpi.Value = 175;

            this.textBoxNameContent.Clear();
            this.buttonUploadContent.Enabled = false;
        }

        private void buttonUploadTiff_Click(object sender, EventArgs e)
        {
            this.textBoxPdfPath.Clear();
            this.textBoxTiffPath.Clear();
            this.fileServiceContent.ClearImage();

            if (openFileDialogContent.ShowDialog() == DialogResult.OK)
            {
                this.textBoxTiffPath.Text = openFileDialogContent.FileName;
                this.textBoxNameContent.Text = openFileDialogContent.SafeFileName;//.Replace(".tiff", "");
                this.buttonUploadContent.Enabled = true;
                this.ShowResultMessagesAndErrors(this.fileServiceContent.UploadTiff(openFileDialogContent.FileName));
            }
        }

        private void buttonUploadContent_Click(object sender, EventArgs e)
        {
            if (this.textBoxTiffPath.Text != null && this.textBoxTiffPath.Text.Length > 0 
                && this.fileServiceContent.DataContent != null)
            {
                MessageBox.Show("Загрузка новоего контента в базу данных.\nНажмите \"ОК\" для продолжения", "Загружаю",
                    MessageBoxButtons.OK);
                ResultOperation resultUpload = fileServiceContent.UploadSelectedTiffInDb(this.textBoxNameContent.Text);
                if (resultUpload.success)
                {
                    this.ResetFormFields();
                }
                this.ShowResultMessagesAndErrors(resultUpload);
            }
            else
            {
                MessageBox.Show("Укажите файл для загрзуки", "Ошибка", MessageBoxButtons.OK);
            }
        }

        private void ShowResultMessagesAndErrors (ResultOperation resultOperation)
        {
            foreach (var message in resultOperation.InfoMessages)
            {
                MessageBox.Show(message, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            foreach (var error in resultOperation.ErrorMessages)
            {
                MessageBox.Show(error, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textBoxNameContent_TextChanged(object sender, EventArgs e)
        {
            if (this.textBoxNameContent.Text.Length >= 3)
            {
                this.buttonUploadContent.Enabled = true;
                return;
            }
            this.buttonUploadContent.Enabled = false;
        }

        private async void UpdateUsersKeys()
        {
            richTextBox1.Invoke(new Action(() => richTextBox1.Clear()));

            string res = "";

            foreach (var item in kuznechikCryptoService.UsersKeys)
            {
                res += string.Format(
                    "------------------\n" +
                    "Время ключа: {0}\n" +
                    "Логин пользователя: {1}\n" +
                    "ID ключа: {2}\n" +
                    "Сеансовый ключ: {3}\n" +
                    "Ключ для контента: {4}\n" +
                    "Нужно обновить ключ?: {5}\n" +
                    "------------------\n\n",
                    item.Value.dateTimeSpanForKeyContent.ToString("dd.MM.yyyy HH:mm:ss"),
                    item.Value.login,
                    item.Key,
                    item.Value.SessionKey.ToHexString(),
                    item.Value.KeyForContent.ToHexString(),
                    item.Value.UpdateKeys ? "Да" : "Нет");
            }

            richTextBox1.Invoke(new Action(() => richTextBox1.Text = res));

            await Task.Delay(Server.Properties.Settings.Default.delayUpdateKeysView);
            await Task.Run(new Action (() =>  UpdateUsersKeys()));
        }
    }
}