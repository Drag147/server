﻿using NLog;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json;
using Server.Services.Crypto.Helper;
using System.IO;
using System.IO.Compression;
using DrmCommon.Response;
using DrmCommon.Request;

namespace Server.ServerScript
{
    class Server
    {
        public delegate void AcceptedDataDelegate(Request request);
        public event AcceptedDataDelegate AcceptedData;

        private Socket _serverSocket;
        private int _port { get { return _port; } set { _port = value; } }
        private long maxBytesForResive;
        private List<ConnectionInfo> _connections = new List<ConnectionInfo>();

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public Server()
        {

        }

        private class ConnectionInfo
        {
            public Socket Socket;
            public byte[] Buffer;
        }

        public bool Start(IPAddress ipListenServer,
            int portServer,
            int numberAsyncOperation = 15,
            long maxBytesForResive = 104857600  //100 мб
            )
        {
            try
            {
                SetupServerSocket(ipListenServer, portServer, maxBytesForResive);
                for (int i = 0; i < numberAsyncOperation; i++)
                {
                    _serverSocket.BeginAccept(new
                        AsyncCallback(AcceptCallback), _serverSocket);
                }
                logger.Info("///Сервер запущен///");
                logger.Info("Прослушивается IP: " + ipListenServer.ToString());
                logger.Info("Прослушивается порт: " + portServer);
                logger.Info("Тип протокола: " + _serverSocket.ProtocolType.ToString());
                logger.Info("Кол-во асинхронных слушателей: " + numberAsyncOperation);
                logger.Info("Максимальное число одновременных соединений: " + (int)SocketOptionName.MaxConnections);
                logger.Info("Максимальный объём принимаемых данных МБ: " + (int)maxBytesForResive / (1024 * 1024));
                logger.Info("///---///");
                return true;
            }
            catch (Exception exept)
            {
                logger.Error("Exception: " + exept);
                return false;
            }
        }

        private void SetupServerSocket(IPAddress ipListenServer, int portServer, long maxBytesForResive)
        {
            this.maxBytesForResive = maxBytesForResive;

            IPEndPoint myEndpoint = new IPEndPoint(
             ipListenServer, portServer);

            // Создаем сокет, привязываем его к адресу
            // и начинаем прослушивание
            _serverSocket = new Socket(
                myEndpoint.Address.AddressFamily,
                SocketType.Stream,
                ProtocolType.Tcp);
            _serverSocket.Bind(myEndpoint);
            _serverSocket.Listen((int)SocketOptionName.MaxConnections);
        }

        private void AcceptCallback(IAsyncResult result)
        {
            ConnectionInfo connection = new ConnectionInfo();
            try
            {
                // Завершение операции Accept
                Socket s = (Socket)result.AsyncState;
                connection.Socket = s.EndAccept(result);
                connection.Buffer = new byte[this.maxBytesForResive];
                lock (_connections) _connections.Add(connection);

                logger.Info("///New Connection///");

                // Начало операции Receive
                connection.Socket.BeginReceive(
                    connection.Buffer,
                    0,
                    connection.Buffer.Length,
                    SocketFlags.None,
                    new AsyncCallback(ReceiveCallback),
                    connection);

                //Новая операции Accept
                _serverSocket.BeginAccept(new AsyncCallback(
                    AcceptCallback), result.AsyncState);
            }
            catch (SocketException exc)
            {
                CloseConnection(connection);
                Console.WriteLine("Socket exception: " +
                    exc.SocketErrorCode);
            }
            catch (Exception exc)
            {
                CloseConnection(connection);
                Console.WriteLine("Exception: " + exc);
            }
        }

        private void ReceiveCallback(IAsyncResult result)
        {
            ConnectionInfo connection = (ConnectionInfo)result.AsyncState;
            try
            {
                int bytesRead = connection.Socket.EndReceive(result);
                if (0 != bytesRead)
                {
                    lock (_connections)
                    {
                        byte[] res = new byte[bytesRead];
                        Array.Copy(connection.Buffer, res, bytesRead);

                        string serialzString = "";

                        if (Properties.Settings.Default.gzipCompress)
                        {
                            serialzString = Encoding.GetEncoding(1251).GetString(Decompress(res));
                        }
                        else
                        {
                            serialzString = Encoding.GetEncoding(1251).GetString(res);
                        }


                        logger.Info("Прияната строка: " + serialzString);
                        JsonSerializerSettings settings = new JsonSerializerSettings
                        {
                            Formatting = Formatting.Indented,
                            TypeNameHandling = TypeNameHandling.Objects
                        };
                        settings.Converters.Add(new BigIntegerConverter());
                        Request request = JsonConvert.DeserializeObject<Request>(serialzString,
                            settings);
                        if (!request.waitResponse)
                        {
                            request.socket = null;
                            CloseConnection(connection);
                        }
                        else
                        {
                            request.socket = connection.Socket;
                        }
                        AcceptedData?.Invoke(request);
                        //отправляем ответ если надо
                        //connection.Socket.Send(Encoding.Default.GetBytes("00:02"), SocketFlags.None);
                    }
                    //ждём ответ при необходимости
                    /*connection.Socket.BeginReceive(
                        connection.Buffer, 0,
                        connection.Buffer.Length, SocketFlags.None,
                        new AsyncCallback(ReceiveCallback),
                        connection);*/
                }
                else
                {
                    //если ответа нет закрываем соединение
                    CloseConnection(connection);
                }
            }
            catch (SocketException exc)
            {
                CloseConnection(connection);
                logger.Error("Socket exception: " + exc.SocketErrorCode);
            }
            catch (Exception exc)
            {
                CloseConnection(connection);
                logger.Error("Exception: " + exc);
            }
        }

        public void SendData(Socket socket, Response response)
        {
            lock (_connections)
            {
                foreach (ConnectionInfo connection in _connections)
                {
                    if (connection.Socket.Equals(socket))
                    {
                        JsonSerializerSettings settings = new JsonSerializerSettings
                        {
                            Formatting = Formatting.Indented,
                            TypeNameHandling = TypeNameHandling.Objects
                        };
                        settings.Converters.Add(new BigIntegerConverter());

                        byte[] contentData = null;
                        if (response is ResponseGetOpenContent responseGetOpen)
                        {
                            contentData = Properties.Settings.Default.gzipCompress 
                                ? Compress(responseGetOpen.content.ContentData)
                                : responseGetOpen.content.ContentData;
                            responseGetOpen.content.ContentData = null;

                            responseGetOpen.sendedSizeContent = contentData.Length;
                        }

                        string responseSerialization = JsonConvert.SerializeObject(response,
                            settings);
                        connection.Socket.Send(
                            Properties.Settings.Default.gzipCompress ?
                            Compress(Encoding.GetEncoding(1251).GetBytes(responseSerialization))
                            : Encoding.GetEncoding(1251).GetBytes(responseSerialization), SocketFlags.None);

                        if (response is ResponseGetOpenContent && contentData != null)
                        {
                            connection.Socket.Send(contentData, contentData.Length, SocketFlags.None);
                        }

                        CloseConnection(connection);
                        break;
                    }
                }
            }
        }

        private void CloseConnection(ConnectionInfo ci)
        {
            ci.Socket.Close();
            lock (_connections) _connections.Remove(ci);
        }

        public byte[] Compress(byte[] data)
        {
            using (var compressedStream = new MemoryStream())
            {
                using (var zipStream = new GZipStream(compressedStream, CompressionMode.Compress))
                {
                    zipStream.Write(data, 0, data.Length);
                    zipStream.Close();
                    return compressedStream.ToArray();
                }
            }
        }

        public byte[] Decompress(byte[] data)
        {
            using (var compressedStream = new MemoryStream(data))
            {
                using (var zipStream = new GZipStream(compressedStream, CompressionMode.Decompress))
                {
                    using (var resultStream = new MemoryStream())
                    {
                        zipStream.CopyTo(resultStream);
                        return resultStream.ToArray();
                    }
                }
            }
        }
    }
}
