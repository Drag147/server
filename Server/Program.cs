﻿using ImageMagick;
using System;
using System.Windows.Forms;

namespace Server
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            MagickNET.SetGhostscriptDirectory(AppDomain.CurrentDomain.BaseDirectory);
            Application.Run(new MainWindow());
        }
    }
}
